- name: Quality Hiring Actual vs Plan
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Quality".
  target: 24 by November 1, 2019
  org: Quality Department
  health:
    level: 3
    reasons:
    - Engineering is on plan. But we are lending some of our recruiters to sales for
      this quarter. And we just put in place a new "one star minimum" rule that might
      decrease offer volume.
  sisense_data:
    chart: 8610186
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY21 planning process
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Quality has been at 31% of plan for Non-Headcount spend.
  urls:
  - https://app.periscopedata.com/app/gitlab/633240/Quality-Non-Headcount-BvAs
- name: Quality Department New Hire Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've fluxuated above and below the target line recently, which for a small
      department is not worrisome
  sisense_data:
    chart: 9389208
    dashboard: 719540
    embed: v2
- name: Quality Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are at our target of 0.58 exactly overall, but trending upward.
    - We need to get the target location factors in the charts.
    - It will probably be cleaner if we get this in periscope.
    - We need to set the target location factors on our vacancies and make sure recruiting
      is targeting the right areas of the globe on a per role basis.
  sisense_data:
    chart: 7045895
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality Overall Handbook Update Frequency Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching <code>/source/handbook/engineering/quality/**</code>over
    time.
  target: 1.1
  org: Quality Department
  is_key: true
  health:
    level: 1
    reasons:
    - Still under target, continuing to champion small iterations and being handbook first
    - Modified KPI to be a rate based metric for department team size.
  sisense_data:
    chart: 10586749
    dashboard: 621059
    shared_dashboard: 3f0e7d87-fc13-44c3-bbba-1a9feb48c3bd
    embed: v2
- name: Quality Department Narrow MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Quality Department <a href="/handbook/engineering/#merge-request-rate">MR
    Rate</a> is a key indicator showing how productive our team members are based
    on the average MR merged per team member. We currently count all members of the
    Quality Department (Director, EMs, ICs) in the denominator because this is a team
    effort. The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts.
  target: Above 10 MRs per Month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Upgraded to attention since we hit our target of 10 in July 2020
    - Working on increasing Quality codebase maintainers and champion smaller iterations
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/534
  sisense_data:
    chart: 8467527
    dashboard: 654023
    shared_dashboard: 4278f770-7709-4a5e-89f7-812a319c2fbb
    embed: v2
- name: Quality Discretionary Bonus Rate
  base_path: "/handbook/engineering/quality/performance-indicators/index.html#quality-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual
    GitLab team members who really shine as they live our values. Our goal is to award
    discretionary bonuses to 10% of GitLab team members in the Quality department
    every month.
  target: At 10%
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - We are consistent at meeting our bonus targets
  sisense_data:
    chart: 9524996
    dashboard: 516343
    embed: v2
- name: Review App deployment success rate for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of our test tooling to enable engineering efficiency.
  target: Above 99%
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Remains above 90%, at 95% for January
    - Increasing review app utilization or stability has been de-prioritized in favor of pipeline improvements, engineering metrics, and Community Contribution workstreams
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/605
  - https://gitlab.com/groups/gitlab-org/-/epics/606
  sisense_data:
    chart: 6721558
    dashboard: 516343
    embed: v2
- name: GitLab project master pipeline success rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of the GitLab project <code>master</code> pipelines
    to accelerate cycle time of merge requests, continuous deployments and GitLab
    EE releases.
  target: Above 95%
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Success rate has been at or above 90% for 10 consecutive months
    - Increased coverage for master broken resolution by Development department has helped keep above 90%
    - Merge train was piloted for `gitlab-org/gitlab` for 8 hours and identified blockers to work through
    - Recent seg fault error has been increasing flaky failure rate
  urls:
  - https://app.periscopedata.com/app/gitlab/564156/Engineering-Productivity---Pipeline?widget=8324723&udv=895971
  - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/195#known-issues-improvements
  - https://gitlab.com/gitlab-org/gitlab/-/issues/11951
  sisense_data:
    chart: 8573283
    dashboard: 516343
    embed: v2
- name: Average successful merge request pipeline duration for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average successful duration of our merge request pipelines to accelerate
    our development cycle time, and continuous deployments. See epics <a href="https://gitlab.com/groups/gitlab-org/-/epics/1853">1853</a>
    and <a href="https://gitlab.com/groups/gitlab-org/-/epics/25">25</a> for individual
    work items.
  target: Below 30 minutes
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Increased from 58.6 to 59.5 due to <a href="https://gitlab.com/gitlab-org/gitlab/-/issues/321680">cache population error</a> which has been <a href="">corrected</a>.
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/1853
  sisense_data:
    chart: 6782964
    dashboard: 516343
    embed: v2
- name: Average time to failure for GitLab merge request pipelines
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the feedback time for merge request pipeline failures to quantify how quickly the feedback is provided to team members that there is a failure which needs action.
  target: Below 30 minutes
  org: Quality Department
  is_key: true
  health:
    level: 0
    reasons:
    - Performance indicator will be implemented with https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10116.
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10116
  sisense_data:
    chart: 11047190
    dashboard: 516343
    embed: v2
- name: Average cost per merge request pipeline for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the cost per pipeline to measure engineering efficiency. This
    is calculated by taking the total cost of all merge request pipelines divided
    by the number of pipelines.
  target: Below "$1.75"
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Adjusted target based on results of <a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9499">recent KR</a>. We are hitting a point of diminishing returns where the risk or cost incurred to gain cost savings is high.
    - Small gains from Average merge request pipeline duration improvements
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9499
  - https://gitlab.com/groups/gitlab-org/-/epics/3806
  sisense_data:
    chart: 8346949
    dashboard: 516343
    embed: v2
- name: Average total pipeline cost per merge request for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the total pipeline cost per merge request to measure engineering efficiency. This
    is calculated by taking the total cost of all pipelines divided by the number of merge requests.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Need to define target
  sisense_data:
    chart: 9366855
    dashboard: 564156
    embed: v2
- name: Bug Service Level Objective (SLO)
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the number of closed bugs within the <a href="https://about.gitlab.com/handbook/engineering/quality/issue-triage/#severity">severity SLO</a>. Bugs are charted by closed month.
  target: TBD
  org: Quality Department
  sisense_data:
    chart: 10471849
    dashboard: 576726
    embed: v2
  is_key: false
  health:
    level: 1
    reasons:
    - Generally above 80% for S1 and between 60% and 70% for S2 bugs, looking to measure improvement with more nudges to EMs/PMs added with <a href="https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/426">https://gitlab.com/gitlab-org/quality/triage-ops/-/issues/426</a>
  urls:
  - https://gitlab.com/groups/gitlab-org/-/epics/1362
- name: Open and closed product severity 1 and severity 2 bugs per month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Tells us the monthly create and resolve of high severity bugs aggregated from each product group.
  target:
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We have started to measure this, need to make this a PI
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/7237
- name: MTTC S1 bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Mean time to close of severity 1 bugs. Measured from time severity label is applied to bug closure.
  target: Below 30 days
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Decreased in December 2020 from 26.8 to 13.4 and 3 month moving average has decreased from 23.2 to 20.8.
    - Future revision to bug PIs to add average age and bug SLO percent achieved
  urls:
    - https://app.periscopedata.com/app/gitlab/829861/
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9951
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9950
  sisense_data:
    dashboard: 829861
    chart: 11115910
    shared_dashboard: 7d9c1811-f0d7-4028-a2b2-03f4f46f6431?
    embed: v2
- name: MTTC S2 bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Mean time to close of severity 2 bugs. Measured from time severity label is applied to bug closure.
  target: Below 60 days
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - There have been bug cleanups which are closing a larger proportion of old S2 bugs. Contextual charts were added to the shared dashboard
  urls:
    - https://app.periscopedata.com/app/gitlab/829831/
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10909
  sisense_data:
    dashboard: 829831
    chart: 11115401
    shared_dashboard: c0879d8a-4933-4019-9825-25575f16ddd7
    embed: v2
- name: Open S1 and S2 bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Identifies the cumulative number of open S1 and S2 bugs per month.
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - 17 open `~severity::1` ~bug past SLO (30 days)
      - 390 open `~severity::2` ~bug past SLO (90 days)
  sisense_data:
    dashboard: 576726
    chart: 9841791
    embed: v2
- name: Average Age of Open S1 Bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average age calculates the average time a bug has been open as of the reported day. Number of bugs is shown to provide context to Average Age. This performance indicator is scoped to S1 open bug issues for all is_part_of_product projects on a daily aggregation for the past 1 year. 
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
      - Age is a new metric and being viewed in relation to Mean time to close and untriaged bugs. Target and health will be updated when the performance indicator matures.
  sisense_data:
    dashboard: 736012
    chart: 10963450
    embed: v2
- name: Average Age of Open S2 Bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average age calculates the average time a bug has been open as of the reported day. Number of bugs is shown to provide context to Average Age. This performance indicator is scoped to S2 open bug issues for all is_part_of_product projects on a daily aggregation for the past 1 year. 
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - Age is a new metric and being viewed in relation to Mean time to close and untriaged bugs. Target and health will be updated when the performance indicator matures.
  sisense_data:
    dashboard: 736012
    chart: 10963420
    embed: v2
- name: Percent of bugs with unknown severity
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percent known to unknown severity on open bugs.
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Have had more than 100 unseveritized open ~bug issues since 2019-02-01
  sisense_data:
    dashboard: 576726
    chart: 9987812
    embed: v2
- name: Unique Community Contributors per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Distribution of unique authors for all merged Community contribution MRs by month.
  target: Above 150 contributors per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9522755
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Community MR Coaches per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: The number of MR Coaches defined by team.yml role
  target: Above 50 coaches per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - Continual growth of MR coaches across different Engineering functions.
  sisense_data:
    chart: 9721107
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10519
- name: Community Contribution Mean Time to Merge
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average days from creation to merge for Community contribution MRs by month
  target: Below 10 days
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9555034
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Percent of Feature Community Contribution MRs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percentage of merged `~"Community contribution"` MRs that are labelled `~feature`
  target: Above 30%
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Making progress towards goal of 30% by improving Community Contribution automation and improve issue grooming to prep feature refinement issues for Community Contributors
  sisense_data:
    chart: 9640193
    dashboard: 729542
    embed: v2
    aggregation: monthly
    daterange:
      days: 365
- name: Software Engineer in Test Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Software Engineers in Test against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 46 Software Engineers in Test
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 34% of target
  sisense_data:
    chart: 9668867
    dashboard: 516343
    embed: v2
- name: Engineering Productivity Engineer Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Engineering Productivity Engineers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 16 Engineering Productivity Engineers
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 25% of target
  sisense_data:
    chart: 9669589
    dashboard: 516343
    embed: v2
- name: Quality Engineering Manager Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Quality Engineering Managers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 7 Quality Engineering Managers
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently at 57% of target
  sisense_data:
    chart: 9669154
    dashboard: 516343
    embed: v2
- name: Average duration of end-to-end test suite execution on CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average duration of our full QA/end-to-end test suite in
    the <code>master</code> branch to accelerate cycle time of merge requests, and
    continuous deployments.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven't started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/198
- name: Ratio of quarantine vs total end-to-end tests in CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability and effectiveness of our QA/end-to-end tests
    running in the <code>master</code> branch.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/199
- name: New issue first triage SLO
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure our speed to triage new issues. We currently have ~400 new issues
    every week in CE/EE. We need to go through all of them and identify valid issues
    and high severity bugs.
  target: Below 5 days
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet. We have made progress on fanning out first
      triage to Engineers in the Quality Department.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
    - Fan out triaging to all of Engineering and not just the Quality Department.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/136
  - https://gitlab.com/groups/gitlab-data/-/epics/50
