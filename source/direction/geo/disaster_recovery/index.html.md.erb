---
layout: markdown_page
title: "Category Strategy - Disaster Recovery"
description: "GitLab want disaster recovery to be robust and easy to use for systems administrators - especially in a potentially stressful recovery situation. Learn more!"
canonical_path: "/direction/geo/disaster_recovery/"
---

- TOC
{:toc}

## 🚨 Disaster Recovery

**Last updated**: 2020-12-23

### Introduction and how you can help

- [Overall Strategy](/direction/geo/)
- [Roadmap for Disaster Recovery](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo&label_name[]=geo%3A%3Aactive&label_name[]=Category%3ADisaster%20Recovery)
- [Maturity: <%= data.categories["disaster_recovery"].maturity.capitalize %>](/direction/maturity/)
- [Documentation](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/)
- [Complete Maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/3574)
- [Viable Maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1507)(closed)
- [All Epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo&label_name[]=Category%3ADisaster%20Recovery)

GitLab installations hold business critical information and data. The Disaster
Recovery (DR) category helps our customers fulfill their business continuity
plans by creating processes that allow the recovery of a GitLab instance following a
natural or human-caused disaster. DR complements GitLab's [Reference Architectures](https://about.gitlab.com/solutions/reference-architectures/) and
utilizes [Geo nodes](https://docs.gitlab.com/ee/administration/geo/replication/)
to enable a failover in a disaster situation. We want DR to be
robust, complete and easy to use for [systems
administrators](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator) - especially in a potentially stressful recovery situation.

Please reach out to Fabian Zimmer, Product Manager for the Geo group
([Email](mailto:fzimmer@gitlab.com)) if you'd like to provide feedback or ask
any questions related to this product category.

This strategy is a work in progress, and everyone can contribute. Please comment and contribute in the linked
[issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo&label_name[]=Category%3ADisaster%20Recovery)
and [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo&label_name[]=Category%3ADisaster%20Recovery)
on this page. Sharing your feedback directly on GitLab.com is the best way to
contribute to our strategy and vision.

### Overview

⚠️ Currently, there are [some
limitations](https://docs.gitlab.com/ee/administration/geo/replication/index.html#current-limitations)
of what data is replicated. Please make sure to check the documentation!
{: .alert .alert-warning}

Setting up a Disaster Recovery solution for GitLab requires significant
investment and is cumbersome in more complex setups. [Geo replicates around 80% of GitLab's data](https://docs.gitlab.com/ee/administration/geo/replication/datatypes.html#limitations-on-replicationverification), which means that systems administrators
need to be aware of what is automatically covered and what parts need to be backed up separately, for example via `rsync`. Geo provides documentation for [planned and unplanned failover processes](https://docs.gitlab.com/ee/administration/geo/disaster_recovery/planned_failover.html).

The Geo group completed [all work required to increase the category from minimal to viable](https://gitlab.com/groups/gitlab-org/-/epics/1507). After evaluating user experience scores, we will decide if the maturity of Disaster Recovery is consistent with viable maturity.

### Where we are headed

In the future, our users should be able to use a GitLab Disaster Recovery
solution that fits within their business continuity plan. Users should be able
to choose which Recovery Time Objective (RTO) and Recovery Point Objective (RPO)
are acceptable to them and GitLab's DR solution should provide configurations
that fit those requirements.

A systems administrator should be able to confidently setup a DR solution even
when the setup is complex, as is the case for reference architectures that support several thousand users. In case of an actual disaster, a systems administrator should be able to follow a simple and
clear set of instructions that allows them to recover a working GitLab
installation - in most cases a failover should be fully automatic and require minimal user intervention!

In order to ensure that DR works, failovers should be tested on a regular basis with
minimal interruption to end-users.

We envision that GitLab's Disaster Recovery processes and solution should

- cover different scenarios based on acceptable Recovery Time Objective (RTO) and Recovery Point Objective (RPO). There is always a trade off between the complexity of the system needed given the requirements in a disaster recovery. GitLab's DR strategies should make this explicit to users.
- support all [reference architectures](https://docs.gitlab.com/ee/administration/reference_architectures/) - from small installations with hundreds of users to extremely large installations with millions of users.
- by default allow the recovery of *all* customer relevant data that was available on the production instance. Users should not need to think about caveats or exclusions.
- be as simple to execute as possible. All instructions should fit on one laptop screen (< 10 steps) that are linear and easy to follow.
- allow for planned failover testing that ensure DR is fully functional.
- be complemented by monitoring that can detect a potential disaster.
- be actively used on GitLab.com to ensure that all best practices are followed and to ensure that we dogfood our own solutions.

### Target audience and experience

#### [Sidney - (Systems Administrator)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sidney-systems-administrator)

- 🙂 **Minimal** - Sidney can manually configure a DR solution using Geo nodes. More complex configurations, such as HA, are supported but are highly manual to set up. Some data may not be replicated. Failovers are manual.
- 😊 **Viable** - Sidney can follow a set of clearly defined procedures for planned failovers. DR is available for all reference architectures; some data may not be replicated yet.
- 😁 **Complete** - Sidney can choose between different configurations that clearly link back to suggested RTO and RPO requirements. Configuration is simple and all solutions are constantly monitored. A dashboard informs users of the current status. A recovery process is less than <10 steps.
- 😍 **Lovable** - Automatic failovers are supported.

For more information on how we use personas and roles at GitLab, please [click
here](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/).

### What's Next & Why

A complete overview of work required to reach complete maturity is available in the [Disaster Recovery Complete maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/3574).

#### Maintenance mode

As part of a planned failover process, it is required to fully synchronize a primary Geo site and a secondary site so that no data is lost. We are working on [a maintenance mode](https://gitlab.com/groups/gitlab-org/-/epics/2149) that blocks any write operations on the primary site thereby allowing both sites to fully get in sync. Additionally, a maintenance period may be useful in other situations e.g. during upgrades or other infrastructure changes.

#### Improved data verification

As of December 2020, [Geo replicates 80% of all data](https://docs.gitlab.com/ee/administration/geo/replication/datatypes.html#limitations-on-replicationverification); however, not
all data is automatically verified. We've created a [self-service framework](https://docs.gitlab.com/ee/development/geo/framework.html) that
supports replication strategies for Git repositories and blobs (files). We are
adding blob verification support to the framework, with [package files being supported first](https://gitlab.com/groups/gitlab-org/-/epics/1817).

#### Support for PostgreSQL clusters

The Geo Primary site supports [high-availability configuration of PostgreSQL using Patroni](https://docs.gitlab.com/ee/administration/postgresql/replication_and_failover.html#configuring-patroni-cluster); however, Geo secondary sites have only experimental support for a similar configuration. This is problematic because it means that a failover to a designated secondary site can't utilize a high-availability configuration immediately. Exactly mirroring the architecture on the primary and secondary site is not yet possible. We are working on making [PostgreSQL clusters generally available](https://gitlab.com/groups/gitlab-org/-/epics/2536).

#### Create a new Geo site overview page

We've worked with systems administrators and are going to [design and implement a new Geo overview page](https://gitlab.com/groups/gitlab-org/-/epics/4712).

### In a year

#### Enable Geo on GitLab.com for Disaster Recovery

GitLab.com is by far the largest GitLab instance and is used by GitLab to
[dogfood GitLab
itself](https://about.gitlab.com/handbook/engineering/index.html#dogfooding).
GitLab.com does not use GitLab Geo for DR purposes. This has many
disadvantages and the Geo Team is [working with Infrastructure to enable Geo on
GitLab.com](https://about.gitlab.com/company/team/structure/working-groups/disaster-recovery/).

### What is not planned right now

We currently don't plan to replace PostgreSQL with a different database e.g.
CockroachDB.

### Maturity plan

This category is currently at the <%=
data.categories["disaster_recovery"].maturity %> maturity level, and our next
maturity target is viable (see our [definitions of maturity
levels](/direction/maturity/)).

In order to move this category from  <%=
data.categories["disaster_recovery"].maturity %> to viable we finished all work in the
[viable maturity epic](https://gitlab.com/groups/gitlab-org/-/epics/1507). We are going to evaluate
UX scores and then update the maturity level.

### Metrics

We currently track [the total number of replication events](https://app.periscopedata.com/app/gitlab/500159/Enablement::Geo-Metrics?widget=10278011&udv=0), which scales with the overall amount of data and our ability to replicate more data types.

### Competitive landscape

GitHub Enterprise Server 2.22 supports a [passive replica server](https://docs.github.com/en/enterprise-server@2.22/admin/enterprise-management/configuring-high-availability-replication-for-a-cluster) that can be used for disaster recovery purposes.

### Analyst landscape

We do need to interact more closely with analysts to understand the landscape
better.

### Top customer success/sales issue(s)

- [Geo secondary sites support PostgreSQL clusters](https://gitlab.com/groups/gitlab-org/-/epics/2536)
- [Maintenance mode](https://gitlab.com/groups/gitlab-org/-/epics/2149)

### Top user issues

- [Category issues listed by popularity](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=group%3A%3Ageo&label_name[]=Category%3ADisaster%20Recovery)

### Top internal customer issues/epics

- [Geo for DR on GitLab.com](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/12)

### Top strategy item(s)

- [Geo secondary sites support PostgreSQL clusters](https://gitlab.com/groups/gitlab-org/-/epics/2536)
- [Maintenance mode](https://gitlab.com/groups/gitlab-org/-/epics/2149)
