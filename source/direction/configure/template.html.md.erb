---
layout: markdown_page
title: "Product Direction - Configure"
description: "GitLab's Configure stage solves user problems related to the configuration and operation of applications and infrastructure. Learn more!"
canonical_path: "/direction/configure/"
---

- TOC
{:toc}

The mission of the Configure stage is to make developers and operators more productive and efficient in executing configuration and operational tasks for cloud-native applications and infrastructure. The Configure stage is closely related release management and [continuous delivery](/direction/release/continuous_delivery/). We do this by providing integrated workflows all within GitLab, a complete [Value Stream Delivery Platform](https://www.gartner.com/document/3979558).

Our vision is that DevOps teams will use GitLab as their primary day-to-day tool as it will provide first-class operator support.

- See a high-level
  [roadmap](https://gitlab.com/groups/gitlab-org/-/roadmap?scope=all&utf8=✓&state=opened&label_name[]=devops%3A%3Aconfigure).

### Target personas

We build solutions targeting the following personas:

* [Application Operator](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
* [Platform engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#allison-application-ops)
​
On small teams, these personas may be just one individual, while on larger teams they may be dedicated roles.

In keeping with our [single application](https://about.gitlab.com/handbook/product/single-application/) promise, we want
GitLab to be a robust, best-of-breed tool for operators as much as it is for developers. Our vision is that operators
will use GitLab as their main day-to-day tool for provisioning, configuring, testing, and decomissioning infrastructure.

### Pricing

GitLab's Configure stage is focused on providing enterprise ready configure capabilities to enable users to operate and deploy modern applications and cloud infrastructure. However, since all application code needs a form of infrastructure, there will continue to be basic offering that targets Free users.

#### Free

Every customer, including individual developers, need to be able to configure and deploy infrastructure. As such, integration with Terraform for Infrastructure as code is available to all GitLab users.

Cloud native development is container based. More and more are using Kubernetes for container orchestration, even on personal projects. As part of the GitLab Free offering, Kubernetes management is also available with a limit to manage a single cluster.

#### Premium

For medium sized organizations, limits on clusters under management are lifted so that teams can use GitLab GitOps without restriction. In addition as the infrastructure complexities increase, as part of our policy management capability, Premium users can see and visualize policy violations to provide safeguards to teams operating quickly.

#### Ultimate

For enterprises, when the complexity in infrastructure becomes unmanageable manually, Configure will provide more insights and visualization into various infrastructure elements. This includes cluster cost management and the ability to visualize the whole infrastructure.

### Strategic directions

We build products primarily for an 8 team SaaS product company, likely with a serverless or docker based architecture.
In the latter case, we assume it's deployed either to Kubernetes or with Terraform to anything that Terraform supports.

### Challenges

We understand GitLab will play side-by-side with existing tools which teams have already invested considerable time and
money. As a result, we will extend GitLab's Configure features to provide an outstanding, integrated experience with the
preferred tools in the industry. Keeping up with the quickly changing nature of these tools is our primary challenge.

The technologies where we want to provide an outstanding, integrated experience with are

- Kubernetes deployment, management, and monitoring
- Terraform usage, planning, and application
- Configuration management behind Terraform scripts

### Opportunities

Our opportunities in the Configure stage are derived from the [Ops Section opportunities](/direction/ops/#opportunities).
Namely:

* **IT operations skills gap:** Operations skills are difficult to attract. We'll make it easier for you to leverage your
operations expertise by efficiently managing your infrastructure platform.
* **Clear winner in Kubernetes:** Our focus on enabling cloud-native applications based on the winner in container
orchestration and management, Kubernetes, means you can leverage the broader Cloud Native ecosystem as you adopt modern
application architectures.
* **Infrastructure as Code:** is the second most looked after investment target in 2021. As more and more companies turn their 
attention towards automating their infrastructures, they need support in transitioning and operating large scale, code-provisioned
fleets.

This leads us to look into common workflows that we would like to support with a market-leading experience:

1. Flexible and robust Kubernetes cluster management
1. Infrastructure as Code, Policy as Code
1. Secrets Management

Other categories that fall under the Configure direction, but we are not actively investing in are

1. Serverless
1. Runbooks
1. ChatOps
1. GitOps

## Product principles

We aim to achieve these by focusing on the following principles.

### Embrace the cloud-native ecosystem

We want to be good cloud-native citizens, build on top of and contribute back into open source tools. We believe in the power 
of the open source community and GitLab's everyone can contribute ethos.

### Aim for experienced cloud users, but make it easy to get started for new users

We understand that Infrastructure as Code and cluster management at scale are complex, and best of breed technologies and much 
customization is required to fulfill advanced workflows. We want to support such advanced use cases. At the same time, we believe
that many new users will become advanced users, and we can support them as well by providing production ready, turn-key solutions that incorporate
the best practices followed by experts.

### Provide an integrated experience

At GitLab we build a single application for the whole Dev(Sec)Ops pipeline. Our solutions should integrate deeply with and should support other GitLab
features. We are paying special attention to security and collaboration oriented features.

### Be enterprise ready

While we want to provide supporting products for every company size, we expect enterprise users to have special needs that our integrated approach can serve well. 
Focusing on their use cases we can reduce their costs and enable faster go to market.

## Performance Indicators (PIs)

Our [Key Performance Indicator](https://about.gitlab.com/handbook/ceo/kpis/) for the Configure stage is the **Configure
SMAU** ([stage monthly active users](https://about.gitlab.com/handbook/product/performance-indicators/#stage-monthly-active-users-smau)).

Configure SMAU is a proxy metric of all the active project users for any projects with a cluster attached.                                                    |

See the corresponding [Sisense dashboard](https://app.periscopedata.com/app/gitlab/511813/Configure-team-business-metrics) (internal) for our primary KPIs.

## Auto DevOps

Our vision for “[Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)” is
to leverage our [single application](/handbook/product/single-application/) to
assist users in every phase of the development and delivery process,
implementing automatic tasks that can be customized and refined to get the best
fit for their needs.

With the dramatic increase in the number of projects being managed by software
teams (especially with the rise of micro-services), it's no longer enough to
just craft your code. In addition, you must consider all of the other aspects
that will make your project successful, such as tests, quality, security,
logging, monitoring, etc. It's no longer acceptable to add these things only
when they are needed, or when the project becomes popular, or when there's a
problem to address; on the contrary, all of these things should be available at
inception.

That's why we created Auto DevOps. We also made it composable so you can pick and choose which [stages](https://docs.gitlab.com/ee/topics/autodevops/#stages-of-auto-devops) to use our default templates in.
The relevant [Auto DevOps components](https://docs.gitlab.com/ee/topics/autodevops/#using-components-of-auto-devops) for each GitLab stage are maintained by the product groups in those stages.
For example the [Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/#auto-deploy) [jobs](https://gitlab.com/gitlab-org/gitlab/-/tree/master/lib%2Fgitlab%2Fci%2Ftemplates%2FJobs) are maintained by the Progressive Delivery group and the [Auto SAST](https://docs.gitlab.com/ee/topics/autodevops/#auto-sast), [Dependency Scanning](https://docs.gitlab.com/ee/topics/autodevops/#auto-dependency-scanning), [License Compliance](https://docs.gitlab.com/ee/topics/autodevops/#auto-license-compliance), [Container Scanning](https://docs.gitlab.com/ee/topics/autodevops/#auto-container-scanning) and [DAST](https://docs.gitlab.com/ee/topics/autodevops/#auto-dast) jobs are [maintained by the relevant Secure groups](/direction/secure/#auto-devops).

[Watch the video explaining our vision on Auto DevOps](https://www.youtube.com/watch?v=KGrJguM361c)

[Learn more](/product/auto-devops/) • [Documentation](https://docs.gitlab.com/ee/topics/autodevops/) • [Direction](/direction/configure/auto_devops/)

## Kubernetes Management

Configuring and managing your Kubernetes clusters can be a complex, time-consuming task.
We aim to provide a simple way for users to configure their clusters within GitLab; tasks
such as scaling, adding, and deleting clusters become simple, single-click events.

[Learn more](/solutions/kubernetes/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/) • [Direction](/direction/configure/kubernetes_management/)

## Infrastructure as Code

Infrastructure as code (IaC) is the practice of managing and provisioning infrastructure through
machine-readable definition files, rather than manual hardware configuration or interactive
configuration tools. The IT infrastructure managed by this comprises both physical equipment
such as bare-metal servers as well as virtual machines and associated configuration resources.
The definitions are stored in a version control system. IaC takes proven coding techniques and
extends them to your infrastructure directly, effectively blurring the line between what is an
application and what is the environment.

Our focus will be to provide tight integration with best of breed IaC tools, such that all
infrastructure related workflows in GitLab are well supported. Our initial focus will be on Terraform.

[Direction](/direction/configure/infrastructure_as_code/)

## Cluster Cost Optimization

Compute costs are a significant expenditure for many companies, whether they
are in the cloud or on-premise. Managing these costs is an important function
for many companies. We aim to provide easy-to-understand analysis of your infrastructure
that could identify overprovisioned infrastructure (leading to waste), recommended changes,
estimated costs, and automatic resizing.

[Direction](/direction/configure/cluster_cost_optimization/)

## Serverless

Taking full advantage of the power of the cloud computing model and container
orchestration, cloud native is an innovative way to build and run applications.
A big part of our cloud native strategy is around serverless. Serverless
computing provides an easy way to build highly scalable applications and
services, eliminating the pains of provisioning & maintaining.

[Learn more](/product/serverless/) • [Documentation](https://docs.gitlab.com/ee/user/project/clusters/serverless/) • [Direction](/direction/configure/serverless/)

## Runbook Configuration

[Incident Management](https://gitlab.com/groups/gitlab-org/-/epics/349) will
allow operators to have real-time view into the happenings of their systems.
Building upon this concept, we envision rendering of runbook inside of GitLab as
interactive documents for operators which in turn could trigger automation
defined in `gitlab-ci.yml`.

[Documentation](https://docs.gitlab.com/ee/user/project/clusters/runbooks/) • [Direction](/direction/configure/runbooks/)

## ChatOps

The next generation of our ChatOps implementation will allow users to have a
dedicated interface to configure, invoke, and audit ChatOps actions, doing it in
a secure way through RBAC.

[Documenation](https://docs.gitlab.com/ee/ci/chatops/) • [Direction](/direction/configure/chatops/)

<%= partial("direction/contribute", :locals => { :stageKey => "configure" }) %>

## Prioritization Process

In general, we follow the same [prioritization guidelines](/handbook/product/product-processes/#how-we-prioritize-work)
as the product team at large. Issues will tend to flow from having no milestone,
to being added to the backlog, to being added to this page and/or a specific
milestone for delivery.

You can see our entire public backlog for Configure at this
[link](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Configure);
filtering by labels or milestones will allow you to explore. If you find
something you're interested in, you're encouraged to jump into the conversation
and participate. At GitLab, everyone can contribute!

Issues with the "direction" label have been flagged as being particularly
interesting, and are listed in the sections below.

## Upcoming Releases

<%= direction["all"]["all"] %>

<%= partial("direction/other", :locals => { :stage => "configure" }) %>
