---
layout: secure_and_protect_direction
title: "Category Direction - Container Network Security"
description: "GitLab's container network security aims to allow any app to run on a cluster with any other app with confidence of defense against any unintended use or traffic"
canonical_path: "/direction/protect/container_network_security/"
---

- TOC
{:toc}

## Protect

| | |
| --- | --- |
| Stage | [Protect](/direction/protect/) |
| Maturity | [Minimal](/direction/maturity/) |
| Content Last Reviewed | `2021-02-25` |

### Introduction and how you can help
<!-- Introduce yourself and the category. Use this as an opportunity to point users to the right places for contributing and collaborating with you as the PM -->

<!--
<EXAMPLE>
Thanks for visiting this category direction page on Snippets in GitLab. This page belongs to the [Editor](/handbook/product/categories/#editor-group) group of the Create stage and is maintained by <PM NAME>([E-Mail](mailto:<EMAIL@gitlab.com>) [Twitter](https://twitter.com/<TWITTER>)).

This direction page is a work in progress, and everyone can contribute:

 - Please comment and contribute in the linked [issues](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=snippets) and [epics]((https://gitlab.com/groups/gitlab-org/-/epics?label_name[]=snippets) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email, Twitter, or on a video call. If you're a GitLab user and have direct knowledge of your need for snippets, we'd especially love to hear from you.
</EXAMPLE>
-->
Thanks for visiting this category direction page on Container Network Security in GitLab. This page belongs to the Container Security group of the Protect stage and is maintained by Sam White ([swhite@gitlab.com](mailto:<swhite@gitlab.com>)).

This direction page is a work in progress, and everyone can contribute. We welcome feedback, bug reports, feature requests, and community contributions.

 - Please comment and contribute in the linked [issues](https://gitlab.com/gitlab-org/gitlab/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Category%3AContainer%20Network%20Security) and [epics](https://gitlab.com/groups/gitlab-org/-/epics/822) on this page. Sharing your feedback directly on GitLab.com is the best way to contribute to our strategy and vision.
 - Please share feedback directly via email or on a video call. If you're a GitLab user and have direct knowledge of your need for container security, we'd especially love to hear from you.
- Can't find an issue? Make a [feature proposal](https://gitlab.com/gitlab-org/gitlab/-/issues/new?issuable_template=Feature%20proposal) or a [bug report](https://gitlab.com/gitlab-org/gitlab/-/issues/new?&issuable_template=Bug). Please add the appropriate labels by adding this line to the bottom of your new issue `/label ~"devops::protect" ~"Category:Container Network Security" ~"group::container security"`.
<!--- https://gitlab.com/gitlab-org/gitlab/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) --->
- Consider signing up for [First Look](https://about.gitlab.com/community/gitlab-first-look/).

We believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-to-gitlab-application) and so if you wish to contribute [here is how to get started](https://about.gitlab.com/community/contribute/).

### Overview
Container Network Security involves filtering and securing the network traffic inside a containerized environment to enforce a least privilege access model and to block attacks at the network layer whenever possible.  Although this category is currently at a [minimal](/direction/maturity/) maturity level, the end goal is to provide a solution that includes the following key features and capabilities:

 - Network firewalling of individual namespaces and pods to limit traffic across a cluster
 - Network signature detection to identify and/or block potentially malicious packets
 - Network IDS/IPS to alert on potentially malicious network activity
 - Network behavior analytics to identify anomalous network behavior

The long-term goal and intent is to support these capabilities across containerized environments.  We plan to start with support for Kubernetes (including self-hosted Kubernetes, GKE, and EKS) and later add support for other cloud containerized environments such as Openshift or serverless.  We do not plan to add support for non-containerized environments.

#### Target Audience
<!--
List the personas (https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas#user-personas) involved in this category.

Look for differences in user's goals or uses that would affect their use of the product. Separate users and customers into different types based on those differences that make a difference.
-->
* [Sam (Security Analyst)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#sam-security-analyst) and [Alex (Security Operations Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#alex-security-operations-engineer) are our primary target personas for any organizations that have an established security team
* [Devon (DevOps Engineer)](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#devon-devops-engineer) could be a secondary persona for organizations without established security teams.  This needs to be researched more.

### Where we are Headed
<!--
Describe the future state for your category.
- What problems are we intending to solve?
- How will GitLab uniquely address them?
- What is the resulting benefits and value to users and their organizations?

Use narrative techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is at least minimally realized. In order to challenge your level of ambition
(with the goal to make it sufficiently high), link to the current market leaders long-term vision and address how
we plan to displace them. -->
We are planning to build a Container Network Security solution that is cloud native, easy to use, and tightly integrated with the rest of GitLab.  Our underlying architecture will combine several technologies to create a full-featured solution while also simplifying and unifying the management experience to look and feel like a single, easy-to-use product.  We plan to be both a network-based IDS and an IPS, allowing users to choose to either log, alert, or block any activity that is detected in their containerized environments.

Some of the top detection and protection capabilities that are planned include network firewalling, segmentation, signature blocking, and behavior analytics.  We plan to provide an intuitive policy editor to simplify the administration of the tool.  We also plan to surface actionable alerts and logs inside GitLab to allow for a simple triage and response workflow to detected attacks.  Longer-term we plan to add support for serverless applications as well as other container management tools beyond Kubernetes.

#### Key goals and guiding principles

1. **Engage security teams** - build a pathway to get security departments involved in GitLab
1. **Add value to open source** - Limit who can make policy changes; support auditing and approvals
1. **Unified experience** - Provide a consistent user experience that abstracts away the choice of underlying technologies
1. **Single app vision** - Tightly integrate with the rest of GitLab to drive cross-stage adoption and to allow scan results to inform enforcement decisions
1. **Nail it then scale it** - Get the solution right for containerized Kubernetes applications first - then extend that support to other platforms (serverless, OpenShift, bare-metal, VMs)

#### What is our Vision (Long-term Roadmap)

To reach the [Viable Maturity](/direction/maturity/) level, we will need to implement the following features:

* [Initial MVC of a Project Level Alerts Dashboard](https://gitlab.com/groups/gitlab-org/-/epics/3438)
* Improved performance monitoring for Container Network Policies
* Ability to view Container Network Policy logs in the GitLab UI

#### What's Next & Why (Near-term Roadmap)
<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/product-processes/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->
In [13.9](https://gitlab.com/groups/gitlab-org/-/milestones/56), we released a [Security Alert Dashboard](/releases/2021/02/22/gitlab-13-9-released/#security-alert-dashboard-for-container-network-policy-alerts) and extended our policy editor to allow users to configure Network Policies policies to send an Alert to GitLab.

We now plan to temporarily defer future work on this category in favor of allocating our resources toward the [Security Orchestration](/direction/protect/security_orchestration/) and [Container Scanning](/direction/protect/container-scanning/) categories.  Community contributions are always welcome, and it is likely that additional Gitlab resources will be put towards this category again at some point in the future.

#### What is Not Planned Right Now
<!-- Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->
We are not currently planning to do the following:
*  Add support for bare-metal or non-containerized environments
*  Add support for no-ops or serverless environments

#### Maturity Plan
<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

This category is currently at the XXXX maturity level, and our next maturity target is YYYY (see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend)).

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->
[Planned to Viable](https://gitlab.com/groups/gitlab-org/-/epics/2108)

### User success metrics
<!--
- What specific user behaviors are indicate that users are trying these features, and solving their problems?
- How will users discover these features?
-->
We plan to measure the success of this category based on the total volume of traffic that is inspected by our Container Network Security solution across our entire customer base.

## Competitive Landscape
Current solutions that offer container network security are point solutions.  GitLab can differentiate from other offerings by providing security that is embedded into GitLab managed Kubernetes clusters and tightly integrated into the rest of the GitLab product.  Some of the current offerings are free, while others are proprietary.

Some of the solutions that provide container network security include the following products (list taken from [eSecurity Planet](https://www.esecurityplanet.com/products/top-container-and-kubernetes-security-vendors.html)):
*  Alert Logic
*  Anchore
*  Aporeto
*  Aqua Security
*  Capsule8
*  NeuVector
*  Qualys
*  StackRox
*  Sysdig
*  Twistlock

Additionally, [Cilium](https://github.com/cilium/cilium) and [Calico](https://www.projectcalico.org/) are popular open source projects that provide Container Network Security capabilities.  GitLab has embedded [Cilium](https://github.com/cilium/cilium) into GitLab to allow users to create [Network Policies](https://docs.gitlab.com/ee/user/clusters/applications.html#install-cilium-using-gitlab-cicd).

## Analyst Landscape
This category is part of the market defined by Gartner as the [Cloud Workload Protection Platforms (CWPP) Market](https://www.gartner.com/en/documents/3906670/market-guide-for-cloud-workload-protection-platforms).
