---
layout: markdown_page
title: Statement of Support
description: "What is and is not within the scope of support at GitLab."
---

The GitLab support team is here to help. This document defines what we support in terms of our products, services, and applications. Part of providing effective support is defining what is outside of the scope of support.

Scope of support, in the simplest terms, is what we support and what we do not. Ideally, we would support everything. However, without reducing the quality of our support or increasing the price of our products this would be impossible. These "limitations" help us to create a more consistent and efficient support experience.

Please understand that any support that might be offered beyond the scope defined here is done
at the discretion of the agent or engineer and is provided as a courtesy.

## On This Page
{:.no_toc}

- TOC
{:toc}

## Self-managed

### Starter, Premium, and Ultimate Users

We will help troubleshoot all components bundled with GitLab Omnibus when used
as a packaged part of a GitLab installation. Any assistance with modifications to GitLab, including new functionality, bug-fixes, issues with alpha features or other code changes should go through the GitLab [issue tracker](https://gitlab.com/gitlab-org/gitlab/issues), triage, and release cycle. Support is not offered for local modifications to GitLab source code.

Your Support Contract will cover support for Beta features. We will automatically triage tickets for Beta features with the lowest priority available.

We understand that GitLab is often used in complex environments in combination with a variety of tools. We'll do best-effort support in debugging components that work alongside GitLab.

If you obtained an Ultimate license as part of GitLab's [Open Source or Education programs](https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
Support (as included with Starter, Premium or Ultimate licenses) is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://gitlab.com/gitlab-com/marketing/community-relations/gitlab-oss/blob/master/README.md#license-and-subscription-details) for additional details.

#### We support the current major version and the two previous major versions
Unless otherwise specified in your support contract, we support the current major version and previous two major versions only. For example, as [`13.x` is the current major version](https://about.gitlab.com/releases/), GitLab installations running versions in the `13.x`, `12.x` and `11.x` series are eligible for support.

GitLab only backports fixes, not features and that too on a limited number of prior releases. For more details please visit the [maintenance policy](https://docs.gitlab.com/ee/policy/maintenance.html).

If you contact support about issues you're experiencing while on an unsupported version, we'll link to this section of the Statement of Support and invite you to upgrade. If you encounter any issues in the course of upgrading to a supported version, please open a new ticket.


#### First-time installation and new features

For assistance with first-time installations and configuration of new features, we highly recommend using our comprehensive [documentation](https://docs.gitlab.com/):

* First-time installation [step by step guide](https://about.gitlab.com/install/)
* [Updating your GitLab instance](https://docs.gitlab.com/ee/update/README.html)
* [High availability](https://docs.gitlab.com/ee/administration/reference_architectures/index.html) 
* [Kubernetes clusters](https://docs.gitlab.com/ee/user/project/clusters/)    

Alternatively, you can reach out to our Professional Services team via the following [page](https://about.gitlab.com/services/).
Our Professional Services team provides training and assistance with design and implementation of new features and installations.

In case you are facing challenges after attempting an installation of a new deployment or implementing new features, a support ticket can be opened using our [support portal](/support/#contact-support).

### Core and Community Edition Users

If you are seeking help with your GitLab Core or Community Edition installation, note that
the GitLab Support Team is unable to directly assist with issues with specific installations
of these versions. Please use the following resources instead:

- [GitLab Documentation](https://docs.gitlab.com): Extensive documentation
regarding the possible configurations of GitLab.
- [GitLab Community Forum](https://forum.gitlab.com/): This is the best place to have
a discussion about your Community Edition configuration and options.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search
for similar issues before posting your own, as there's a good chance somebody else
had the same issue as you and has already found a solution.

Our [community advocates](/handbook/marketing/community-relations/community-advocacy/)
also spend time on the Community Forum and Stack Overflow to help where they can, and
escalate issues as needed.

### Out of Scope

The following details what is outside of the scope of support for self-managed instances with a license.

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| 3rd party applications and integrations | *I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server.* | GitLab Support can help ensure that GitLab is providing properly formatted data to 3rd party applications and integrations. |
| Debugging EFS problems | *GitLab is slow in my HA setup. I'm using EFS.* | EFS and GlusterFS are **not** recommended for HA setups (see our [HA on AWS doc](https://docs.gitlab.com/ee/university/high-availability/aws/)).<br/><br/>GitLab Support can help verify that your HA setup is working as intended, but will not be able to investigate EFS or GlusterFS backend storage issues. |
| Debugging git repository issues stored on NFS (from GitLab 14.0) | *Commits vanished from our `main` branch. We're running 3 Gitaly servers, sharing data using NFS.* | NFS related-issues with Gitaly are supported up to GitLab 13.12, but will not be supported from GitLab 14.0. [Read more about deprecation of Gitaly support for NFS](#gitaly-and-nfs). |
| Troubleshooting non-GitLab Omnibus components | *I'm trying to get GitLab to work with Apache, can you provide some pointers?* | GitLab Support will only assist with the specific components and versions that ship with the GitLab Omnibus package, and only when used as a part of a GitLab installation. | 
| Local modifications to GitLab | *We added a button to ring a bell in our office any time an MR was accepted, but now users can't log in.* | GitLab Support would direct you to create a feature request or submit a merge request for code review to incorporate your changes into the GitLab core. | 
| Old versions of GitLab | *I'm running GitLab 7.0 and X is broken.* | GitLab Support will invite you to upgrade your installation to a more current release. Only the current and two previous major versions are supported. |
| Instance migration configuration and troubleshooting | *We migrated GitLab to a new instance and cannot SSH into the server.* | GitLab Support will assist with issues that arise from the GitLab components. GitLab Support will not be able to assist with any issues stemming from the server or it's configuration (see GitLab Instance Migration on the [Support page](../support/index.html)). |
| Debugging custom scripts and automations | *We use custom scripts to automate changes to our GitLab deployment, and it is causing problems or downtime.* | GitLab Support will assist in troubleshooting and resolving issues that occur in the course of interacting with an existing GitLab installation. GitLab Support will not be able to assist with debugging or fixing customer-written code used to deploy, upgrade or modify an in-place installation. |
| Installation of GitLab using unofficial, community-contributed methods | *We ran into an error installing GitLab using the FreeBSD package. Please help!* | GitLab Support can only provide support for installation problems encountered when using an [official installation method](../install/index.html). |
| Live Upgrade Assistance for GitLab installed using unofficial, community-contributed methods | *We installed GitLab using the Arch Linux community package and would like to request live upgrade assistance* | GitLab Support can only provide Live Upgrade Assistance when GitLab is installed using an [official installation method](../install/index.html). |

#### Gitaly and NFS

From GitLab 14.0, enhancements and bug fixes for NFS for Git repositories will no longer be considered and customer technical support will be considered out of scope. Technical support for deployments of GitLab version 13.x and earlier is unchanged, constrained only by [which versions of GitLab are eligible for support, and GitLab's maintenance poiicy](#we-support-the-current-major-version-and-the-two-previous-major-versions).

As more customers started deploying larger GitLab environments and needing to scale out Gitaly, we learned that NFS was unsuitable for storing git data. It does not perform well with a workload that consists of lots of small files, like git repositories. Under heavy load, NFS features that are designed to improve performance (such as caching) were found to result in different Gitaly servers returning different data for the same git repository. This can result in intermittent inconsistency issues for read requests, and for write requests, we also suspect this as the root cause of some data loss issues.

GitLab developed Gitaly Cluster specifically to meet the needs of scaled-out deployments of GitLab, and customers should upgrade to Gitaly Cluster as soon as possible. For single node installations of GitLab Omnibus or single node Gitaly deployments, use block storage instead of NFS.

We will continue to help with git related tickets from customers running one or more Gitaly server with its data stored on NFS. Examples include:

- Performance issues or timeouts accessing Git data
- Commits or branches vanish.
- GitLab intermittently returns the wrong git data (such as reporting that a repository has no branches).

Assistance will be limited to activities like:

- Verifying developers' workflow uses features like protected branches.
- Reviewing GitLab event data from the database to advise if it looks like a force push over-wrote branches.
- Verifying that NFS client mount options prioritise data integrity over performance, and that certain caching options are set correctly (such as `lookupcache=positive`)
- Analysing the GitLab Workhorse and Rails logs, and determining that `500` errors being seen in the environment are caused by slow responses from Gitaly.

GitLab support will be unable to progress the investigation further if:
- The GitLab deployment is v14 or above, and
- The only remaining avenue for investigation is to look at Gitaly logs off all the servers sharing the same NFS mount point.

If the issue is reproducable, or if it happens intermittently but regularly, GitLab Support will investigate providing the issue reproduces without the use of NFS. The affected respositories need to be migrated to a different Gitaly shard, such as Gitaly cluster or a standalone Gitaly VM, backed up by block storage.

## GitLab.com

### Bronze, Silver, and Gold Users

GitLab.com has a full team of Site Reliability Engineers and Production Engineers monitoring its status 24/7. This means that often, by the time you notice something is amiss, there's someone already looking into it.

We recommend that all GitLab.com customers follow [@gitlabstatus](https://twitter.com/gitlabstatus) on Twitter and use our [status page](https://status.gitlab.com) to keep informed of any incidents.

If you obtained a Gold subscription as part of [GitLab's Open Source or Education programs](https://about.gitlab.com/blog/2018/06/05/gitlab-ultimate-and-gold-free-for-education-and-open-source/),
support is **not** included unless purchased separately. Please see the [GitLab OSS License/Subscription Details](https://gitlab.com/gitlab-com/marketing/community-relations/opensource-program/blob/master/README.md#licensesubscription-details) for additional details.

### Free Plan Users

Technical and general support for those using our free options is “Community First”.
Like many other free SaaS products, users are first directed to find support in community
sources such as the following:

- [GitLab Documentation](https://docs.gitlab.com): Extensive documentation
on anything and everything GitLab.
- [GitLab Community Forum](https://forum.gitlab.com): Get help directly from the community. When able, GitLab employees also participate and assist in answering questions.
- [Stack Overflow](http://stackoverflow.com/questions/tagged/gitlab): Please search
for similar issues before posting your own, as there's a good chance somebody else
had the same issue as you and has already found a solution.

#### GitLab.com Availability for Free Users
You should follow [@gitlabstatus](https://twitter.com/GitLabStatus) on Twitter for status
updates on GitLab.com, or check [our status page](https://status.gitlab.com/) to see if there is a known service outage and follow the linked issue for more detailed updates.

#### Support for Free Plan Users
The GitLab.com support team _does_ offer support for 

- broken states for specific users or repositories (for example "I get a 500 every time I log in, my friend doesn't.")
- requests related to an email you received related to a security or production incident.

For help with these issues please open a [support request](https://support.gitlab.com).
For *free* GitLab.com users seeking support, a support agent or engineer may determine that the request
is more appropriate for community forums or the issue tracker than for the official GitLab Support Team.

Note that issues affecting paid users receive a higher priority. There are no guaranteed response times associated with free accounts.

### Out of Scope

The following details what is outside of the scope of support for GitLab.com customers with a subscription.

| Out of Scope       | Example        | What's in-scope then?   |
|--------------------|----------------|-------------------------|
| 3rd party applications and integrations | *I can't get Jenkins to run builds kicked off by GitLab. Please help me figure out what is going on with my Jenkins server* | GitLab Support can help ensure that Gitlab is providing properly formatted data to 3rd party applications and integrations in the bare-minimum configuration. |
| Troubleshooting non-GitLab components | *How do I merge a branch?* | .com Support will happily answer any questions and help troubleshoot any of the components of GitLab |
| Consulting on language or environment-specific configuration | *I want to set up a YAML linter CI task for my project. How do I do that?* | The Support Team will help you find the GitLab documentation for the related feature and can point out common pitfalls when using it. |

## Out of Scope for All Tiers

The following are outside of the scope of support for both users of Self-managed GitLab and GitLab.com with or without a license or subscription.

### General
{:.no_toc}

1. **Training**: Support is unable to provide training on the use of the underlying technologies that GitLab relies upon. GitLab is a product aimed at technical users, and Support expects our users and customers be versed in the basic usage of the technologies related to features that they're seeking support for. For example, a customer looking for help with a Kubernetes integration should understand Kubernetes to the extent that they could retrieve log files or perform other basic tasks without in-depth instruction.

### Git
{:.no_toc}

1. `git` specific commands and issues (not related to GitLab)

### CI/CD
{:.no_toc}

1. Helping debug specific commands or scripts in a `.gitlab-ci.yml`
1. Issues other than configuration or setup of private runner **hosts**

_**Note:** If you would like help with a GitLab CI/CD feature, we recommend creating a test branch with a minimal `.gitlab-ci.yml` that illustrates the problem. In most cases this will enable our team help you more quickly than trying to debug a complex pipeline described by a large `.gitlab-ci.yml` file._
