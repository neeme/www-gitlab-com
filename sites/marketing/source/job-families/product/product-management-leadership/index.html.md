---
layout: job_family_page
title: "Product Management - Leadership"
description: "This job family represents all Managers in the Product Management department."
---

This job family represents all Managers in the Product Management department. They are responsible for managing, directly or indirectly GitLab team members performing in the role of [Product Manager](/job-families/product/product-manager/). Outside of the [VP of Product Management](#vp-of-product-management), these roles are responsible for managing and building teams which focus on [specific specialties](#specialties) as part of GitLab's [product hierarchy](/handbook/product/categories/#hierarchy) ([sometimes encompassing entire stages or a section](/handbook/product/product-leadership/#product-organizational-structure)). 

## References
* [Product Handbook](/handbook/product/)
* [Product Leadership](/handbook/product/product-leadership/)
* [Product Development Workflow](/handbook/product-development-flow/)
* [Product Management Career Development Framework](/handbook/product/product-manager-role/#product-management-career-development-framework)
* [Engineering Workflow](/handbook/engineering/workflow)
* [Product Handbook](/handbook/product/)

## Responsibilities
* Make sure you have a great product team (recruit and hire, sense of progress, promote proactively, identify underperformance)
* Work on the vision with the [Product Leadership](/handbook/product/product-leadership/) and CEO; and communicate this vision internally and externally
* Distill the overall vision into a compelling roadmap
* Make sure the vision advances in every release and communicate this
* Communicate our vision though demo's, conference speaking, blogging, and interviews
* Work closely with Product Marketing, Sales, Engineering, etc.

## Requirements
* Ability to use GitLab
* Experience hiring teams in high growth companies
* Experience in DevOps
* Experience with solutions from the product category, groups or DevOps stages you will be responsible for
* Technical background or clear understanding of developer products; familiarity with Git, Continuous Integration, Containers, Kubernetes, and Project Management software a plus
* You are living wherever you want and are excited about the [all remote](/company/culture/all-remote/) lifestyle
* You share our [values](/handbook/values/), and work in accordance with those values
* [Leadership at GitLab](/company/team/structure/#director-group)

## Levels

### Group Manager, Product (GMP)
This role typically manages 2-4 Product Managers, and reports either to a [Director of Product](#director-of-product-management), a [Senior Director of Product Management](#senior-director-of-product-management) or the [VP of Product Management](#vp-of-product-management).

#### Group Manager, Product (GMP) - Job Grade 
The Group Manager, Product is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Group Manager, Product (GMP) - Responsibilities
**Team responsibilities**
* Ensure that the next milestone contains the most relevant items to customers, users, and us
* Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
* Make sure the [DevOps tools](/devops-tools/) are up to date
* Keep relevant [/direction](/direction/) pages up to date as our high level roadmap
* Regularly join customer and partner visits that spawn ideas for new features
* Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
* Make sure the release announcements are attractive and cover everything
* Prioritize, build and assess business cases for new or existing opportunities in your group that have no investment. 
* Be present on social media (hacker news, twitter, stack overflow, mailinglist), especially around releases

_Please note: If the Group Manager has less than 4 direct reports or is a first-time manager, some individual contributor responsibilities may be expected to be maintained._

#### Group Manager, Product (GMP) - Requirements
* 1-3 years experience managing others
* 4-6 years of experience in product management
* Additional requirements are outlined in the [Product Management Career Development Framework](/handbook/product/product-manager-role/#product-management-career-development-framework)

#### Group Manager, Product (GMP) - Career Ladder
The next step in the Group Manager, Product is to move to [Director of Product](#director-of-product-management). 

#### Group Manager, Product (GMP) - Specialties

##### Fulfillment
Responsible for the [Fulfillment](/direction/fulfillment/) product team, which owns delivering a flexible and powerful billing and licensing system.  Has the opportunity to work cross-functionally to ensure the billing and licensing system meets our evolving business needs and delivers a world class customer experience.

**Requirements**
* Demonstrated business operations skills and an ability to think systematically about complex workflows and work cross-functionally to ensure an excellent end-to-end customer experience
* Previous experience with high scale billing & licensing systems
* Familiarity with commercial best practices for no touch, sales assisted, and partner assisted transactions
* Excellent communication skills at all levels, including e-group

##### Verify
Are responsible for managing the team of product managers covering the [Verify stage groups](/handbook/product/categories/#verify-stage). Leading the [Verify direction](/direction/ops/#verify) is a high impact position. Works with your team to ensure our [industry-leading CI](/resources/forrester-wave-cloudnative-ci/) continues to be a [critical entry-point](/handbook/marketing/product-marketing/usecase-gtm/ci/) to our single devops platform. Work cross functionally as an expert and invest in your team of high caliber product managers to make sure our [R&D investment in Verify](/handbook/product/investment/#investment-by-stage) is as effective as possible in fulfilling our [mission to enable everyone to contribute](/company/strategy/#mission).

##### Enablement
Responsible for the [Geo](https://about.gitlab.com/handbook/product/categories/#geo-group), [Global Search](https://about.gitlab.com/handbook/product/categories/#global-search-group), [Memory](https://about.gitlab.com/handbook/product/categories/#memory-group), and [Database](https://about.gitlab.com/handbook/product/categories/#database-group) groups. These groups ensure GitLab can meet customer business continuity requirements, is performant at all scales, and enables users to find the content they are looking for in our single platform.

**Requirements**
* Demonstrated understanding of software systems, distributed systems a plus
* Previous experience with self-managed products and their lifecycle operations
* Strong communications skills and ability to work cross-functionally, as these groups affect many parts of GitLab

#### Group Manager, Product (GMP) - Hiring Process
Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. 

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with a Director of Product
* Next, candidates will be invited to schedule a 45 minute peer interview with an Engineering Manager
* Next, candidates will be invited to schedule a 45 minute deep dive interview with a member of the Product team
* Next, candidates will be invited to schedule a 45 minute direct report panel interview with members of the Product team
* Next, candidates will be invited to schedule a 45 minute fifth interview with a VP of Product or above

### Director of Product Management
Director of Product Management reports to either a [Senior Director of Product Management](#senior-director-of-product-management) or the [VP of Product Management](#vp-of-product-management).

#### Director of Product Management - Job Grade
The Director, Product Management is a [grade 10](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Director of Product Management - Responsibilities
**Team responsibility**
* Ensure that the next milestone contains the most relevant items to customers, users, and us
* Work with customers, users, and other teams to make feature proposals enticing, actionable, and small
* Make sure the [DevOps tools](/devops-tools/) are up to date
* Keep relevant [/direction](/direction/) pages up to date as our high-level roadmap
* Regularly join customer and partner visits that can spawn ideas for new features
* Ensure that we translate user demands to features that make them happy but keep the product UI clean and the codebase maintainable
* Make sure the release announcements are attractive and cover everything
* Be present on social media (hacker news, twitter, stack overflow, mailing-list), especially around releases

#### Director of Product Management - Requirements
Requirements for the role are outlined as part of the [Product Career Development Framework](/handbook/product/product-manager-role/#product-management-career-development-framework).
* [Validation Track](/handbook/product-development-flow/#validation-track) Skills
    - Ensures consistent execution of discovery track skills across a large team
* [Build Track](/handbook/product-development-flow/#build-track) Skills
    - Ensures consistent execution of build track skills across a large team.
    - Responsible for health of working relationships with Engineering Directors.
* Business Skills
    - Works cross [product hierarchy](/handbook/product/categories/#hierarchy) and cross-functionally to ensure an excellent end-to-end customer experience.
    - Excellent at understanding and managing business impact across a wide range of product domains.
    - Capable of making key pricing and packaging recommendations.
* Communication Skills
    - Visible leader across teams.
    - Establishes compelling team purpose that is aligned to overall organizational vision.
    - Inspires broader team to achieve results.
    - Identifies disconnects to vision and takes appropriate action.
* [People Management](/company/team/structure/#director-group) Skills
    - Aligns team with larger [Section](/handbook/product/categories/#devops-stages) vision and goals.
    - Provides appropriate level of guidance and latitude to managers and team members.
    - Experienced at hiring and at managing out underperformance.
    - Excellent at caring personally for team members and providing candid real-time feedback.
* Ten to twelve years of experience
* Four years of people management experience

#### Director of Product Management - Career Ladder
The next step for a Director of Product Management is to move to [Senior Director of Product Management](#senior-director-of-product-management).

#### Director of Product Management - Specialties

##### Sec
The Director of Product, Sec is the PM DRI for the Secure parts of the
[DevOps lifecycle](/handbook/product/categories/#sec-section)
and reports to the VP of Product Management.

##### Growth
The Director of Product, Growth is the PM DRI for the Growth parts of the
[DevOps lifecycle](/handbook/product/categories/#growth-section)
and reports to the Chief Product Officer.

##### Enablement
The Director of Product, Enablement is the PM DRI for the Enablement parts of the
[DevOps lifecycle](/handbook/product/categories/#enablement-section)
and reports to the VP of Product Management.

#### Director of Product Management - Hiring Process
Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. 

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with the VP of Product Management
* Next, candidates will be invited to schedule a 45 minute second peer interview with a Product Director
* Next, candidates will be invited to schedule a 45 minute third interview with another member of the Product team
* Next, candidates will be invited to schedule a 45 minute fourth peer interview with an Engineering Director
* Next, candidates will be invited to schedule a 45 minute fifth interview with a Product or Engineering Director or above
* Next, candidates will be invited to schedule a 45 minute Deep Dive interview with a Product Director
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO

### Senior Director of Product Management
Senior Director of Product Management reports to the [VP of Product Management](#vp-of-product-management).

#### Senior Director of Product Management - Job Grade
The  Senior Director, Product Management is a [grade 11](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Director of Product Management - Requirements
* Effective at hiring, coaching, and leading a team of 10+ that typically spans numerous stages, and is composed of Directors, Group Managers, and IC's
* Demonstrated ability to manage and coach product validation in brand new market areas
* Skilled at curating excellent cross-stage customer experiences
* Experience and skill managing products at all stages of the lifecycle (incubation, growth, maturity, decline), and balancing priorities and investment accordingly  

#### Senior Director of Product Management - Responsibilities
* Visible strategic leader across the company
* Skilled at big stage communications
* Capable of coaching team members on excellent communication skills 

#### Senior Director of Product Management - Career Ladder
The next step in the Product Management Leadership job family is to move to the [VP of Product Management](/job-families/product/vp-of-product-management) job family.

#### Senior Director of Product Management - Specialties

##### Dev
The Director of Product, Dev is the PM DRI for the Dev parts of the
[DevOps lifecycle](/handbook/product/categories/#dev-section)
(e.g manage, plan, and create) and reports to the VP of Product Management.

##### Ops
The Director of Product, Ops is the PM DRI for the Ops parts of the
[DevOps lifecycle](/handbook/product/categories/#ops-section)
(e.g. Monitor, Configure, Verify, Package, and Release) and reports to the VP of Product Management.

#### Senior Director of Product Management - Hiring Process
Candidates for this position can generally expect the hiring process to follow the order below. Note that as candidates indicate preference or aptitude for one or more [specialties](#specialties), the hiring process will be adjusted to suit. Please keep in mind that candidates can be declined from the position at any stage of the process. 

* Selected candidates will be invited to schedule a 30 minute [screening call](/handbook/hiring/#screening-call) with one of our Technical Recruiters
* Next, candidates will be invited to schedule a 60 minute first interview with the VP of Product Management
* Next, candidates will be invited to schedule a 45 minute second peer interview with a Product Director
* Next, candidates will be invited to schedule a 45 minute third interview with another member of the Product team
* Next, candidates will be invited to schedule a 45 minute fourth peer interview with an Engineering Director
* Next, candidates will be invited to schedule a 45 minute fifth interview with a Product or Engineering Director or above
* Next, candidates will be invited to schedule a 45 minute Deep Dive interview with a Product Director
* Finally, candidates may be asked to schedule a 50 minute final interview with our CEO

### VP of Product Management
The VP of Product Management reports to the [Chief Product Officer](/job-families/product/evp-of-product/).

#### VP of Product Management - Job Grade

The VP of Product is a [grade 12](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### VP of Product Management - Responsibilities
* Play a key role in helping the GitLab Product Management team scale rapidly and realize our product vision to be a complete DevOps platform.
* Hire, lead, and coach a rapidly growing team of 30+ Product Managers
* Manage a team of 4-6 Product Directors
* Ensure a cohesive, coherent, and compelling end-to-end customer experience
* Align team with end-to-end product line vision and goals
* Leverage portfolio product management techniques to ensure product investments are properly allocated across the end-to-end GitLab product
* Partner effectively with Engineering, Design, and Product Marketing to ensure we validate, build, launch, and measure product experiences that customers love and value
* Help refine and implement the GitLab [product development flow](/handbook/product-development-flow/), ensuring team members receive training and coaching required to work effectively within the system
* Serve as a spokesperson for the end-to-end GitLab product internally and externally

#### VP of Product Management - Requirements
* 18 years+ of relevant experience, with 10+ years of people management experience, including management of Directors+
* Demonstrated understanding of DevOps markets, competition, and underlying technologies
* Track record of leading products to successful commercial outcomes
* Excellent at boardroom and big stage presentations, and able to inspire and motivate customers and employees through written and verbal communications
* Demonstrated ability to teach and coach the product management skills as outlined [here](/handbook/product/product-manager-role/#product-management-career-development-framework)

#### VP of Product Management - Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).

* Apply on the GitLab Jobs [page](/jobs/)
* 30 minute screening call with Recruiter
* 50 minute interview with EVP, Product
* 50 minute panel interview with Sr. Director, Engineering & Director, UX
* 50 minute interview with Director, Product
* 50 minute interview with Director, Product
* 50 minute interview with CEO

Additional details about our process can be found on our [hiring page](/handbook/hiring).
 
## Performance Indicators
* [Relevant Product Performance Indicators](https://about.gitlab.com/handbook/product/performance-indicators/#structure)
* [Product KPI's](/handbook/business-ops/data-team/metrics/#product-kpis)

### Leadership-levels Performance Indicators 

* Product Managers job satisfaction (Coming Soon) 
* IACV attained by [use case](https://about.gitlab.com/handbook/use-cases/#specific-use-cases) as captured in the [Command Plan](https://app.periscopedata.com/app/gitlab/705822/Product-Management-+-Sales-Opportunity-Feedback) 
* Completion of competitive analysis of [product adjacencies](https://www.mckinsey.com/industries/technology-media-and-telecommunications/our-insights/grow-fast-or-die-slow-pivoting-beyond-the-core#) 
* Delivery of cross-stage adoption Opportunity Canvases, such as [Why SCM users do not adopt CI?](https://docs.google.com/document/d/10AqH6kh1b1wSV61e9Hi3XQiLvt3zzdGxhq0Pi4T5m1U/edit#heading=h.4mt5fmtn0ax4)

#### Group Manager Performance Indicators 

* 1 per quarter adjacencies analysis for Stage OR 
* 1 per quarter cross-stage opportunity canvas 


