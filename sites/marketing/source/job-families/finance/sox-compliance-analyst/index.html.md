---
layout: job_family_page
title: Internal Auditor - SOX
---

The Internal Auditor – SOX has the responsibility of executing the assessment of Internal Control Over Financial Reporting including Entity Level Controls, IT General and Application Controls and Process Controls in scope for CEO/CFO Certification.

## Responsibilities
- Involved in Sarbanes-Oxley (SOX) 404 compliance requirements and testing of internal controls.
- Interacts with managers and finance team to document the process and conducts testing of internal controls on an ongoing basis.
- Applies knowledge of internal control concepts and experience to support the Internal Audit team with performing the evaluation of various business processes.
- May support the operational audits to ensure compliance with company policies and regulations.

## Requirements
- Excellent verbal and written communication skills with the ability to interact effectively with all levels of management.
- Demonstrated problem-solving abilities with customer service orientation.
- Self-starter and flexible team player.
- Ability to work in a fast-paced environment with changing processes and procedures.
- Strong project management abilities.
- Must have moderate SOX compliance experience and be knowledgeable with the following financial cycles: Record to Report, Order to Cash, Hire to Retire, Procure to Pay
- Comprehension of internal auditing standards, Sarbanes-Oxley, COSO and risk-assessment practices.
- Must be able to work during US and Canada timezones with the overlap of at least 4 hours
- Degree in Accounting, Business or Finance required
- Technical auditing skills and corporate-level audit experience required
- 5+ years of experience in SOX/internal audit preferred, of which at least 3 years of SOX experience required
- Chartered Accountant/CPA/CIA certification preferred
- Ability to use GitLab

## Performance Indicators
- [Percentage of Desktop procedures documented](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of controls tested](/handbook/internal-audit/#internal-audit-performance-measures)
- [Percentage of recommendations implemented](/handbook/internal-audit/#internal-audit-performance-measures)

## Hiring Process
Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team/).
- Selected candidates will be invited to schedule a 30 minute screening call with our Global Recruiters
- Next, candidates will be invited to schedule a 45 minute interview with our Senior Internal Audit Manager
- Further, candidates will be invited to schedule a 30 minute interview with our Senior Accounting Manager
- Further, candidates will be invited to schedule a 30 minute interview with our Accounting and Financial Reporting Manager
- Candidates will then be invited to schedule a 30 minute interview with our CFO
- Successful candidates will subsequently be made an offer via email
Additional details about our process can be found on our [hiring page](/handbook/hiring).

## Relevant Links
- [Finance Handbook](/handbook/finance)
