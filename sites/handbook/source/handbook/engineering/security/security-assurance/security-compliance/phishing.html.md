---
layout: handbook-page-toc
title: "Phishing Program"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# Phishing Program

The GitLab phishing program provides ongoing phishing simulations and trainings to GitLab team members that enhances knowledge and identification of malicious emails as well as help identify social engineering vulnerabilties where additional education is provided.  Phishing simulations are provided by PhishLabs, GitLab's contracted solution that will help satisfy external regulatory requirements and bolster customer assurance.

## Objective

To provide phishing simulations and trainings to a subset of team members within the GitLab organization, at miminum on a quarterly cadence.

## When will phishing simulations occur?

At minimum, one phishing simulation will take place each fiscal quarter.  

Prior to the phishing simulation taking place, a general notification to the GitLab organization will be posted to the #company-fyi Slack channel (the exact date/time and team members receiving the email will not be communicated).

## Who will receive the phishing simulations?

A subset of the GitLab organization will receive an email from our phishing vendor, PhishLabs. 

## What will a phishing simulation email look like?

The phishing simulation email from Phishlabs will appear as though it is originating from GitLab or a fictitious company.  This email will look realistic/authentic in the attempt to engage the team member to click a link. 

### Phishing email is successful

In the event the link is clicked, the team member will be redirected to a landing page notifying them they failed the simulation.  This action will automatically enroll the end user in a short training session also provided by PhishLabs and a link to the training will be sent to the team member to complete.

### Phishing email is not successful

We sincerly hope all participants identify the phishing simulation test and do one of the following:

* Do nothing and delete the email, and/or
* Forward the phishing email to `phishing@gitlab.com` and/or
* Engage the Security team on the #security Slack channel

Additional information is available on the main Security department handbook page - [What to do if you suspect an email is a phishing attack](/security/#what-to-do-if-you-suspect-an-email-is-a-phishing-attack)

### Phishing Simulation and Training Metrics

We will be utilizing Sisense to create dashboards for reporting to organization - TBD

### Additional Resources

* [How to identify a basic phishing attack](https://about.gitlab.com/handbook/security/#how-to-identify-a-basic-phishing-attack)
* [PhishLabs](https://about.gitlab.com/handbook/business-ops/tech-stack/#phishlabs)

### Questions, Comments, Concerns?

Please reach out to the Security Compliance team! /security-assurance/security-compliance/compliance.html#contact-the-compliance-team
