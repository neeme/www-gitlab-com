---
layout: handbook-page-toc
title: Demonstrations
description: "Solutions Architects are occasionally called on at a moments notice to give a demo or join a call to show a prospect or customer specific GitLab functionality"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

[**SA Practices**](/handbook/customer-success/solutions-architects/sa-practices) - [**Sales Plays**](/handbook/customer-success/solutions-architects/sales-plays) - [**Tools and Resources**](/handbook/customer-success/solutions-architects/tools-and-resources) - [**Career Development**](/handbook/customer-success/solutions-architects/career-development) - [**Demonstration**](/handbook/customer-success/solutions-architects/demonstrations) - [**Processes**](/handbook/customer-success/solutions-architects/processes)

# Demonstrations
{:.no_toc}

## Demo Systems Overview and Specific Environments

The [Demo Systems Handbook page](/handbook/customer-success/demo-systems) provides an overview of the demo system infrastructure used for demonstrating GitLab features, value propositions, and workflows.

See the [Environments sub-page](/handbook/customer-success/demo-systems/environments) for specific details on how to access the environments for use in your demos.

## Demo Readiness

Solutions Architects are occasionally called on at a moments notice to give a demo or join a call to show a prospect or customer specific GitLab functionality. A best practice for being prepared for these demo requests is to have a minimized browser window with various content preloaded into the browser tabs. Examples of common environments utilized are listed below. Note that accessing some of these links are only enabled for GitLab team-members.  (To-do: port this list to the demo catalog)

- The [GitLab Demo Systems](/handbook/customer-success/demo-systems/) provides a catalog of demos and the sandbox infrastructure for performing demos.
- The [GitLab.org Group level](https://gitlab.com/groups/gitlab-org/-/roadmap) clearly represents GitLab Epics, Roadmaps and cross-project issue views used for planning releases
- A [GitLab.com project](https://gitlab.com/jkrooswyk/joel-springsample/boards/579466) driven by Auto DevOps which includes a populated issue board as well as an active Merge Request is highly useful for describing value of workflow utilizing GitLab Auto DevOps for build, test, security and review
- This [GitLab.com project](https://gitlab.com/gitlab-examples/security/security-reports/merge_requests/2) is commonly utilized specifically for security discussions
- The [production monitoring for GitLab.com](https://gitlab.com/charts/gitlab/environments/190276/metrics) is useful for any discussion regarding custom and in-app monitoring
- The [production Grafana instance](https://dashboards.gitlab.com/d/000000159/ci?refresh=5m&orgId=1) showing GitLab Runner details is a great way to represent the art of the possible as it relates to monitoring and dashboards
- The GitLab [Direction](/direction/) page in the Handbook is often beneficial for future-looking product vision questions and discussions about upcoming features
- Optional: [Integrations](/handbook/marketing/strategic-marketing/demo/integrations/) may be loaded and ready to discuss by leveraging the standard demo environments
- Other Existing demo groups and projects are listed in the next section

## Existing Demonstrations

There are a variety of locations for demo groups and projects today. We are in the process of reviewing and cataloging all existing materials as part of the [FY21 Q3 OKR - Establish a well-maintained demonstration repository](https://gitlab.com/gitlab-com/customer-success/solutions-architecture-leaders/sa-initiatives/-/issues/9).

Today, demo groups and projects are split between several distinct locations.  We are in the process of building out a [Demo Catalog Project](https://gitlab.com/gitlab-com/customer-success/solutions-architecture/demo-catalog) to easily search for existing demos.  Please consider contributing to the demo catalog.  Instructions can be found within the project README.  While this is being built out, here are various locations where you will find demo groups and projects:

1. [GitLab Demo Portal Catalog Libraries](https://gitlabdemo.com/catalog/libraries) - provides demo scenarios with corresponding videos and project build out on the GitLab demo environment. Some demo projects also have wiki pages with additional resources, such as a link to a slide presentation, demo script, and objection handling.
1. [GitLab.com](https://gitlab.com) - there are a variety of groups and projects with Content
    - [GitLab Examples](https://gitlab.com/gitlab-examples) are used by product groups for feature validation, speed runs, demos, and feature showcasing in docs. The [GitLab CI/CD Examples documentation](https://docs.gitlab.com/ee/ci/examples/) points our customers to these for their own use as well.

- [Guided Explorations](https://gitlab.com/guided-explorations) - has projects for a variety of topics, from language-specific feature flag examples, various DevOps patterns, pipeline tips, tricks, and hacks for windows, and more.
- [GitLab CS Tools](https://gitlab.com/gitlab-cs-tools) - group that provides projects for GitLab cs automation and migration tools
- [Customer Success Demos](https://gitlab.com/gitlab-com/customer-success/demos) - centralized location for demos used by Customer Success, including a set of golden repos for different application types, security demos, and more.

## Slide Decks for SA Demos

There are several slide decks stored on google drive that are commonly used for demos. Feel free to make a copy of any of these to personalize for your own needs.

- [All the Things](https://docs.google.com/presentation/d/1AG6eDm8USqU7TG12Sp4UEc9B3P03pniQnVHZAGzyDAg/edit?usp=sharing) - contains a comprehensive set of slides on all GitLab functionality and value propositions
- [General Demo.pptx](https://drive.google.com/file/d/17SoRPxPCswT_FublXCsi3rm3TBnHAYI-/view?usp=sharing) - Product walkthrough deck created and maintained by Cherry Han. Tip: you can make a copy of the deck, then open the deck in Google slides to edit it for your own purposes.
