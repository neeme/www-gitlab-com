---
layout: handbook-page-toc
title: "Sandbox Cloud Realm"
---

## On this page
{:.no_toc}

- TOC
{:toc}

### Quick links

#### User Portal
* [gitlabsandbox.cloud](https://gitlabsandbox.cloud)

#### Documentation
* [Global infrastructure standards](/handbook/infrastructure-standards/)
* [Global labels and tags](/handbook/infrastructure-standards/labels-tags/)
* [Infrastructure policies](/handbook/infrastructure-standards/policies/)

#### Issue Tracking and Collaboration
* [HackyStack issue tracking](https://gitlab.com/hackystack/hackystack-portal/-/issues) (open source code feature development)
* [GitLab Sandbox Cloud issue tracking](https://gitlab.com/gitlab-com/sandbox-cloud/issue-tracking/-/issues) (strategic issues)
* `#sandbox-cloud` Slack channel for announcements.
* `#sandbox-cloud-questions` Slack channel to ask questions and get help.

#### Code and Examples
* [GitLab.com gitlab-com/sandbox-cloud group and projects](https://gitlab.com/gitlab-com/sandbox-cloud)
* [Terraform module library](https://gitlab.com/gitlab-com/sandbox-cloud/tf-modules)
* [HackyStack README](https://gitlab.com/gitlab-com/sandbox-cloud/apps-tools/hackystack-portal/-/blob/main/README.md)
* [HackyStack source code](https://gitlab.com/gitlab-com/sandbox-cloud/apps-tools/hackystack-portal)
* [HackyStack screenshots](https://gitlab.com/gitlab-com/sandbox-cloud/apps-tools/hackystack-portal/-/tree/main/docs/screenshots/)

#### Infrastructure Management (ops.gitlab.net)
* [cloud-realms group and projects](https://ops.gitlab.net/cloud-realms)
* [HackyStack source code with CI and Security](https://ops.gitlab.net/cloud-realms/apps-tools/hackystack-portal)
* [HackyStack Runbook Docs for Infra and Security](https://ops.gitlab.net/cloud-realms/apps-tools/hackystack-runbook-docs)
* [gitlabsandbox.cloud - Terraform configuration](https://ops.gitlab.net/cloud-realms/master-account/gcp/gcp-hackystack-portal-prd-tf)
* [gitlabsandbox.cloud - Ansible configuration](https://ops.gitlab.net/cloud-realms/master-account/gcp/gcp-hackystack-portal-prd-ansible)
* [gitlabsandbox.cloud - DNS configuration](https://ops.gitlab.net/cloud-realms/master-account/gcp/gcp-sandbox-cloud-dns-tf)

## Overview

The Sandbox Cloud is an automated provisioning platform for creating an AWS account (today) or GCP project (near future) that you can use for demo/sandbox/testing purposes and is paid for by GitLab with consolidated invoice billing (no credit card required). 

This platform is powered by [HackyStack](https://gitlab.com/gitlab-com/sandbox-cloud/apps-tools/hackystack-portal), an open source project created by [Jeff Martin](https://gitlab.com/jeffersonmartin) to automate the access request process using Okta integration, auto-assigning roles and permissions based on your department, and using the cloud provider API for provisioning your AWS account and/or GCP project.

### How to Get Started

> We are currently at `alpha` stability. Please post in `#sandbox-cloud-questions` if you see unexpected behavior.

1. Visit [https://gitlabsandbox.cloud](https://gitlabsandbox.cloud) and sign in with your Okta account.
1. Navigate to **Cloud Infrastructure** in the top navigation.
1. Click the purple **Create Account** button.
1. Choose the _cloud provider_ and _cloud organization unit_ from the dropdown menu.
1. Click the green **Create Account** button.
1. Your account will take 2-5 minutes for the AWS API to finish the provisioning process while the AWS services are activated for your account.
1. Please refresh your browser window every ~60 seconds until you see that your user account has changed from `Provisioning` to `Active`.
1. Click the **View IAM Credentials** button in the top right corner.
1. Use the provided **URL**, **Username**, and **Password** to sign in to your new AWS account. _Be careful that your browser doesn't autofill saved credentials for a different account._
1. **You're all set!** Please save your credentials in your 1Password private vault for easy access in the future.

> You can sign-in with Okta, however please don’t create a Cloud Account unless you intend to provision AWS resources. You can see the [screenshots](https://gitlab.com/gitlab-com/sandbox-cloud/apps-tools/hackystack-portal/-/tree/main/docs/screenshots/) of everything that a user sees. 

## Background Context and Problem Statement

**The oversimplified user story is "I need to spin up VM(s) or cluster(s) in GCP or AWS to try something (anything, may not be GitLab product specific), what's the company infrastructure standards for doing that?"**

The goal is to create a frictionless approach for technical team members that includes the tagging needed for cost allocation, best practice security configurations, and streamline the provisioning of infrastructure without needing to wait several days or weeks for an access request to be approved and provisioned.

This also reduces the burden on the accounting team that processes expense reports for team members each month. Each team member’s account is now part of consolidated billing.


### History

Over the years, our non-production infrastructure resources have grown organically without accountability, cost controls, naming conventions, provisioning automation, or security best practices. This includes resources created in the GCP gitlab-internal project, AWS gitlab-np account, and DigitalOcean using [dev-resources](https://gitlab.com/gitlab-com/dev-resources). 

### Recent iterations

[Epic 257](https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/257) was created to iterate on our processes. In FY21-Q3, we created company-wide [infrastructure standards](https://about.gitlab.com/handbook/infrastructure-standards/) which solved the “naming things is hard” problem with [labels, tags, and naming conventions](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/) in our AWS and GCP organization accounts. The infrastructure standards define [realms](https://about.gitlab.com/handbook/infrastructure-standards/#gitlab-infrastructure-realms) to create separate security boundary namespaces for different use cases. For our sandbox use cases, we’ve created a [sandbox realm](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox) for individual users and [department realms](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#department-realms) for shared collaboration projects, notably the Engineering Development realm which allows each of the [department groups](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/#gitlab-department-group-gl_dept_group) (functional teams) to have a shared AWS account or GCP project for creating infrastructure.

### Current iteration

Jeff Martin developed the first release of [HackyStack](https://gitlab.com/hackystack/hackystack-portal) that powers the GitLab Sandbox Cloud. We developed the tooling in-house since the existing industry tools only solve 1-3 of the [Technical Problems We're Solving](#technical-problems-that-hackystack-is-solving) and we wanted to automate the workflow end-to-end.

We are developing HackyStack as an open source project to allow other infrastructure or IT team to simplify their processes for provisioning sandbox accounts. HackyStack is not designed for individual use (yet). As we evolve, we'll be able to advocate HackyStack to partners and customers for deploying demo, testing, or training infrastructure without long manual provisioning documentation or burdening internal infrastructure team members.

### Business and financial impact

* All infrastructure resources are associated with a user and department for cost allocation
* Reduced or eliminated expense reports with AWS invoices for individual usage (consolidated billing)
* Budgets and cost controls with Slack notifications to reduce abandoned test environment costs
* Automated access request and provisioning process for IT Ops
* Standardized organizational hierarchy and naming schema for AWS accounts and GCP projects
* Automated security best practice controls and least privilege rights

### Technical problems that HackyStack is solving

1. **Self-Service Provisioning:** Creating an "easy button" for technical users at a company to get access to an AWS account or GCP project with zero manual provisioning by the IT team.
2. **Cloud Agnostic:** Providing a universal interface that is cloud provider agnostic so you don't need to create different architecture and provisioning processes for AWS, GCP, etc.
3. **Hierarchy:** Defining a standard reference architecture for organizational unit hierarchy.
4. **Auto Labeling/Tagging:** Apply labels and tags to resource for cost management, infrastructure-as-code, and security policy compliance without users needing to remember to adding tags.
5. **Billing Costs per User:** Unified billing metrics across all cloud providers on a per-user, per-account/project, and per-group/team level.
6. **Automated Access Requests:** Supplementing single sign-on (SSO) providers with pre-auth automated group membership provisioning with seamless manager approval(s)
7. **Automated Access Approval Provisioning:** Supplementing single sign-on (SSO) providers with post-auth provisioning of infrastructure resources in one or more provider APIs.
8. **GitOps Infrastructure-as-Code Provisioning:** Automatically creating Git projects with Terraform infrastructure-as-code scaffolding with security best practices that uses CI/CD automation.
9. **Standardized Infrastructure-as-Code Library:** Linking to curated library of Terraform modules for easily deploying common infrastructure elements that follow company security best practices.
10. **Daily Workflow Cost Controls:** Slack bots and notifications for users to easily provision or destroy infrastructure and threshold cost/usage report notifications.

### Tech Stack

* [Laravel](https://laravel.com/docs/8.x) - web portal, CLI application, API provisioning handler
* MySQL v5.7 - database
* [Terraform v0.13+](https://www.terraform.io/) - Infrastructure as Code configuration
* [AWS API](https://github.com/aws/aws-sdk-php)
* [Google Cloud ](https://github.com/googleapis/google-api-php-client)
* [GitLab API](https://github.com/GitLabPHP/Client) - For Git SCM of Terraform configurations
* [GitLab CI](https://docs.gitlab.com/ee/ci/) - For automated Terraform deployments

This project was built using Laravel instead of other viable languages due to Jeff's prior experience and proficiency with Laravel to achieve the most efficient time to business value. This builds on the success of the [GitLab Demo Systems](https://about.gitlab.com/handbook/customer-success/demo-systems/) that is powered by the [demosys-portal](https://gitlab.com/gitlab-com/customer-success/demo-systems/infrastructure/demosys-portal). 

For those who are not familiar with Laravel, it is the PHP equivalent of [Ruby on Rails](https://trends.google.com/trends/explore?date=all&q=ruby%20on%20rails,laravel) and [Django](https://trends.google.com/trends/explore?date=all&q=Django,laravel) and has seen [tremendous community popularity](https://packagist.org/packages/laravel/framework/stats) in recent years since PHP has made revolutionary improvements in recent years with PHP 5.x and PHP 7.x. This project also allows us to dogfood GitLab CI/CD capabilities for PHP projects.

### Roadmap

See the issue trackers for the latest up-to-date information.

* [HackyStack issue tracking](https://gitlab.com/hackystack/hackystack-portal/-/issues) (open source code feature development)
* [GitLab Sandbox Cloud issue tracking](https://gitlab.com/gitlab-com/sandbox-cloud/issue-tracking/-/issues) (strategic issues)

#### Current Projects

1. Add GCP project provisioning
1. Create GitOps project per Cloud Account
1. Add Terraform module library for users
1. Deprecate shared AWS accounts with legacy configurations
1. Create new group accounts with automated IAM management
1. Add self-service manager approval for group accounts
1. Add automated access request audit reporting with IT ops issue tracker
1. Add cost and usage reports for AWS and GCP
1. Add Slack bots for easy infrastructure cost reporting and destroy button
1. Implement and automate the GitLab Environment Toolkit (GET)
1. Migrate `dev-resources` infrastructure.

#### FY22 Planning Themes

**Phase 4** - Automated provisioning of AWS accounts and GCP projects for each user and team with streamlined/automated access requests (aka “Automate the manufacturing of everyone’s green LEGO board”). This is being achieved with the HackyStack open source project that Jeff is building.

**Phase 4.5** - Migrate everyone’s resources in shared accounts into respective isolated accounts and apply labels/tags for cost management and reporting.

**Phase 5** - Curate centralized library of Terraform modules, Ansible roles, Packer images, Docker images, and other scripts that have best practice security standards are used for deploying common infrastructure (aka “Provide everyone a box of LEGO bricks and the tools to deploy them”). Integrate GitLab Environment Toolkit for deploying GitLab in decentralized test environments (user sandboxes, community member environments, etc). This will be open source with the community so partners and customer POCs can take advantage of what we have. This will solve Sid’s request for ensuring we’re all on the same page and using the same library for the millions of GitLab users.

**Phase 6** - Create “easy button” for deploying the library of infrastructure (aka the LEGO kits) into a topology builder.

**Product and Revenue Enablement** - Since a lot of provisioning functionality uses GitLab CI/CD and GitOps, we are dogfooding the GitLab product and allows users to manage their infrastructure-as-code in a GitLab repository and extend capabilities with other GitLab features as they see fit.

**HackyStack and GitLab Premium Features** - We will eventually add premium features to HackyStack that would require features included with a GitLab paid subscription.

### How to Contribute

Please post your ideas on Slack or in the issue tracker and so we can discuss the best ways to implement them.

Please direct any questions about the Sandbox Cloud or HackyStack to Jeff Martin or Dave Smith.
