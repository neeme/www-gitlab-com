---
layout: handbook-page-toc
title: "GitLab BV (Netherlands) Benefits"
description: "GitLab BV (Netherlands) benefits specific to Netherlands based team members."
---

Can't find what you're looking for? Try the main [People Operations page](/handbook/people-group/).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to Netherlands based team members

## Vacation Money

Dutch team members get the customary month of vacation money (Vakantiegeld) in the month of May, as defined [by the government](https://www.rijksoverheid.nl/onderwerpen/vakantiedagen-en-vakantiegeld/vraag-en-antwoord/hoe-hoog-is-mijn-vakantiegeld).
Note that Vakantiegeld is built into offers as well as our [Compensation Calculator](/handbook/total-rewards/compensation/compensation-calculator/).

Note that you can choose to have your vacation money distributed throughout the year.
To request a change in the way you receive this, please reach out to peopleops@gitlab.com. [This template](https://docs.google.com/document/d/1IizjvTbZsAz1gCq5uYHlxhVvRW8LaEAlMrbHPHkgs1E/edit) should be completed by a member of the People Operations Specialist team, signed by GitLab and the team member and then send to HRSavvy to request the change.

## Medical

There is no additional medical coverage scheme through GitLab.

Note that in the Netherlands every citizen is obliged to have at least a basic health insurance package (basisverzekering) with a health insurance company. If you do not have health insurance, you can be fined by the Sociale Verzekeringsbank.

GitLab does not plan on adding additional medical cover at this time due to the governmental cover.

## Pension

State pension (AOW) is standard and will be paid out at retirement age. Starting January 1, 2021, GitLab will offer a private pension plan through [Brand New Day](https://new.brandnewday.nl/) for all team members over the age of 21 employed via a GitLab BV Netherlands employment contract. The pension plan will be a Defined Contribution Agreement with a flat rate of 3.00% of the pension base. The GitLab team member will contribute 2%, with the employer (GitLab) contributing 1%. The pension base equals the pensionable salary less the state pension offset.

**Pension Plan Summary:**
* Pension: The Pension plan is designed for team members to build up an invested pension fund that they cash in to purchase a retirement
pension and a partner's pension that pays benefits upon their death after their retirement via monthly contributions. 
* Death Benefits: If a team member dies while in service and before the retirement date, the partner and/or children will receive death benefits. This is a life-long retirement pension for the partner and a temporary dependant's pension for their children which pays benefits on the death of the team member. 
* Incapacity for Work: This is a waiver of premium for incapacity for work. If a team member becomes partially or fully incapacitated for
work, this portion of the plan continues to pay part or all of the pension contributions for team members to keep building up their pension fund and remain covered for the elected benefits. 

**Voluntary Pension Plan Summary:**
* Extra Pension: Team members can make additional voluntary contributions to have extra funds to purchase a retirement pension and a partner's pension that pays benefits upon their death after retirement.
* Extra Death Benefits: Team members with a partner have the option of insuring an extra partner's pension upon their death prior to the retirement date. The following [doc](https://drive.google.com/file/d/1NGD1MNEqYI-acKECh7xNR73su_c-xesK/view?usp=sharing) outlines additional information on how the voluntary ANW-gap pension works. 

**Individual value transfer:**
A team member can have the pension value accrued at a former employer transferred to Brand New Day. An individual value transfer must be requested from Brand New Day within six months of joining the scheme. Once this is complete, the years of service at any former employers count towards Death Benefits. 

**Investment Profile:**
Team members choose their own investment profile: neutral, defensive or offensive or their own mix of equities and bonds, within the limits set by Brand New Day. Furthermore, you can choose to put all the contributions in a savings fund.

## Life Insurance

GitLab does not offer life insurance in the Netherlands at this time. Team members are automatically covered up to 70% of their last wage in case of disability by the governmental Employee Insurance Agency.

## 30% Tax Ruling

Eligible team members may apply for the 30% Tax Ruling. More information can be found on our [Visas](/handbook/people-group/visas/#30-ruling-in-the-netherlands) page. Kindly note, this can also be [expensed](/handbook/people-group/visas/#expensing).

## GitLab B.V. Netherlands Leave Policy

* Statutory Sick Leave
  - Team members who are unable to work due to illness are entitled to 70% of their salary for up to 104 weeks. Team members will receive 100% of their salary for the first six weeks of illness. This leave runs concurrently with GitLab PTO. 
  - GitLab complies with [Dutch local Laws](https://business.gov.nl/regulation/reporting-employee-illness-recovery/) regarding sick leave. Local laws override any leave policy we have in the benefit of the team members.
  - Team members must select `Out Sick` in PTO by Roots to ensure that all sick time is properly tracked. For complete reporting procedures, please review [the sick time procedure for The Netherlands here](/handbook/paid-time-off/#sick-time-procedures---netherlands). 

* Statutory Maternity leave
  - The team member is entitled to a maximum six weeks' leave prior to the estimated date of childbirth and for ten weeks after that date; therefore totaling sixteen weeks.
  - The team member can reduce the leave period prior to the estimated date of childbirth to at least four weeks. In that case, the number of days not taken prior to the estimated date of childbirth is added to the leave period following the estimated date of childbirth.
  - In the event of incapacity for work from six weeks prior to the estimated date of childbirth, the sixteen-week period for pregnancy and childbirth leave commences at that time, regardless of which agreements have been made.
  - Besides this, you’re also entitled to continuous wage during your leave which will still be paid out by GitLab. However, the [UWV](https://www.uwv.nl/overuwv/english/about-us-executive-board-organization/detail/about-us) offers GitLab a WAZO (Work and Care Act) settlement which is destined to cover your salary while on leave. The UWV ensures expert and efficient implementation of team member insurance and the WAZO settlement is one of these insurances. This maternity benefit (WAZO) lasts at least 16 weeks and covers 100% of the daily wage. In order for GitLab to receive this settlement HRSavvy will inform the UWV about your pregnancy via an application form. To apply, please inform peopleops@domain and total-rewards@domain when you’d wish for your maternity leave to start. The exact start date is up to you to decide. Please note that your leave can start 6 weeks prior to, but no later than, 4 weeks before your due date. HRSavvy will then work with you directly to apply and keep peopleops@domain in cc.

* Statutory Parental leave
  - After your partner has given birth you are entitled to up to five days of paid parental leave.
  - Within 6 months of the birth you are also entitled to 5 weeks of additional paid leave at 70% (["Aanvullend geboorteverlof"](https://www.rijksoverheid.nl/onderwerpen/geboorteverlof-en-partnerverlof/geboorteverlof-voor-partners)).
  - Team members who have children under the age of eight are entitled to take unpaid parental leave. In the case of a family with more than one child under the age of eight, that right is applicable for each child. The number of hours' leave is thirteen times the weekly working hours (65 days for full-time employment). However, no more than half of the number of weekly working hours can be taken each week.
  - Please also refer to [the company policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave) as when more beneficial they superside the statutory leave benefits.

* Vacation Leave
  - Team members are entitled to at least 20 vacation days per year which run concurrently with GitLab PTO. The days will accrue at 1.67 days/month from the team member's start date. Any unused days will be carried into the next calendar year, but expire on July 1st. Should the team member leave GitLab within the calendar year, an entitlement to a pro rata part vacation days exists. Team members must designate all vacation days taken as `Vacation` in PTO by Roots to ensure that vacation entitlement is properly tracked.

People Ops will consult with HRSavvy to ensure that the statute is met.

### Applying for Leave in the Netherlands

HRSavvy can assist in applying for maternity leave covered by social security. In this application the company can decide whether the benefit is paid to the team member directly, or the employer continues paying the salary and receives the benefit. The last option is done in most of the cases.
