---
layout: handbook-page-toc
title: Learning Initiatives
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Learning Initiatives Introduction

The Learning & Development team is rolling out learning programs to enable a [culture of curiosity and continual learning](/handbook/values/#growth-mindset). The learning initiatives are intended to provide a forum for team members to come together to learn about all of the activities taking place in the learning space, as well as hear from leaders internally and externally on various leadership topics.

## Learning Sessions

### Live Learning
Live Learning sessions will be conducted on a monthly basis. There will be a Zoom video conference set up for each session. Official dates and topics will be added to the schedule below as confirmed. If you were unable to attend a live learning session but still want to learn, check out our past live learning sessions below. At GitLab we [give agency](/handbook/values/#give-agency), but if you are attending Live Learning sessions, you will be asked to be engaged and participate with your full attention.

<details>
  <summary markdown='span'>
    Format for Live Learning Sessions
  </summary>

<b>Format for 25 minute sessions:</b>

<ul>
<li>10 minutes - introduction/content</li> 
<li>10-15 minutes - Q&A</li>
</ul>

<b>Format for 50 minute sessions (times below are approximate):</b>

<ul>
<li>10-15 minutes - introduction/content</li>
<li>10-20 minutes - breakout session</li>
<li>10-20 minutes - debrief</li>
<li>5 minutes - conclusion</li>
</ul>

</details>

<details>
  <summary markdown='span'>
    Live Learning Schedule
  </summary>

<b>The upcoming Live Learning schedule is as follows:</b>

<ul>
<li>January - 3 Week Manager Challenge</li>
</ul>

</details>


<details>
  <summary markdown='span'>
    Past Live Learning Sessions
  </summary>

<b>2020</b>

<ul>
<li>January - <a href="https://youtu.be/crkPeOjkqTQ">Compensation Review: Manager Cycle (Compaas)</a></li>
<li>January - <a href="/company/culture/inclusion/being-an-ally/">Ally Training</a></li>
<li>February - <a href="/handbook/people-group/guidance-on-feedback/#receiving-feedback">Receiving Feedback</a></li>
<li>June - <a href="/handbook/people-group/guidance-on-feedback/#guidelines-for-delivering-feedback">Delivering Feedback</a></li>
<li>June - <a href="/company/culture/inclusion/unconscious-bias/">Recognizing Bias</a></li>
<li>September - <a href="/handbook/people-group/learning-and-development/manager-challenge/#pilot-program">Manager Challenge Pilot</a></li>
<li>November - <a href="https://www.youtube.com/watch?v=WZun1ktIQiw">Belonging</a></li>
<li>November - <a href="/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge">One Week Challenge - Psychological Safety</a></li>
<li>December - <a href="/handbook/leadership/coaching/#introduction-to-coaching-1">Introduction to Coaching</a></li>
</ul>

<b>2019</b>

<ul>
<li>November - <a href="/company/culture/all-remote/effective-communication/">Communicating Effectively & Responsibly Through Text</a></li>
<li>December - <a href="/company/culture/inclusion/being-inclusive/">Inclusion Training</a></li>
</ul>

</details>


### Social Learning Through Live Learning

Social Learning is the cornerstone for how L&D designs, develops, and delivers live learning sessions. Social learning focuses on how team members interact with peers for just-in-time learning and skill acquisition through knowledge retention. Live learnings serve as an opportunity for team members to build relationships and a sense of community with team members. Social Learning occurs when team members come together in a virtual forum synchronously to learn from others through networking, breakout groups, storytelling, lessons learned reflection, and collaboration on solving scenarios with role playing. The live learnings enables learners to pull knowledge fro experts and peers within the organization instead of having knowledge pushed to them. 

Example of a Social Learning Live Learning Session on [Building High Performing Teams](/handbook/leadership/build-high-performing-teams/)

<figure class="video_container">
  <iframe src="aezVF1nOBWc" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

Social Learning can also occur in GitLab's Learning Experience Platform - [GitLab Learn](https://gitlab.edcast.com/) and asynchronous forums using GitLab. (i.e. [Manager Challenge](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/26))

## Monthly Continuous Learning Call Overview

At GitLab, the Learning and Development (L&D) team hosts a monthly Monthly Continuous Learning call. The series is intended to showcase all of the L&D initatives taking place at GitLab as well as provide a space for team members to ask questions. It's an opportunity to come together as an organization and highlight everything we are doing within the L&D space and to help instill a learning culture at GitLab.

### Goals

1. Spur a culture of curiosity and continuous learning by highlighting all the initiatives we have taking place in learning & development.
1. Increase engagement and improve team member satisfaction and productivity by advancing understanding of available L&D resources

Additional background information on this initiaitve is available in the [Monthly Continuous Learning Call issue](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/96)

### Past Monthly Continuous Learning Call

Check out recordings of previous Learning Speaker Series calls!

- [2020-10-21 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=nfF9OI566uo&list=PL05JrBw4t0Kqz6UXFu_2ELLubpKzg_pOe&index=32)
- [2020-11-18 Monthly Continuous Learning Call](https://youtu.be/tWZ3iVyb-4E)
- [2020-12-16 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=DQdzXFu9Nbc)
- [2021-01-20 Monthly Continuous Learning Call](https://www.youtube.com/watch?v=pnc4FqHk_a0)

### Hosting a Monthly Continuous Learning Call

The following process outlines steps for the L&D team to take each month when planning and implementing monthly calls.

**Planning Issue Template:** Open an Monthly Continuous Learning Call issue template in the L&D Project. Steps outlined below are also included in the issue template and are included here as reference.

1. Post in the [#peopleops Slack channel](https://app.slack.com/client/T02592416/C0SNC8F2N/thread/C0SNC8F2N-1602618225.269200) to coordinate a Zoom meeting on the company calendar. This meeting should be set up to live stream to the GitLab unfiltered YouTube channel. Consider hosting the call at least 2 times to best accommodate for multiple time zones.
1. Create slide deck for presentation. Make a copy of a previous month's presention in the [Continuous Learning Campaign Google Folder](https://drive.google.com/drive/folders/1d4ksJXBMrATATxN0QyJ4FA6hzchMNdvb?usp=sharing)
1. Coordinate slide deck with appropriate enablement audiences (i.e DIB, Field Enablement, Professional Services, Marketing Enablement, etc)
1. Update slide deck for presentation with feedback from coordinated audiences
1. Open a feedback issue for questions and comments to be shared asynchronously.
1. Coordinate an announcement of the call in the #company-fyi Slack channel from Sid or another executive/manager who will be featured that month. The post should be shared 1 business day before the call. This post should include a link to the slide deck and coresponding issue. See notes below for a template that can be shared.

#### Text for CEO share in #company-fyi channel

`Join the Learning and Development team on [DATE] for the Monthly Continuous Learning Call. This month's call is all about [TOPIC]. You can review the slide deck for the call [HERE], and post questions you might have in the call adenga doc [HERE]. Looking forward to seeing you there!`

## Learning Speaker Series Overview

The L&D team hosts a monthly Learning Speaker Series call, open to all team members. The calls serve to provide a space where senior executives and mid-level people leaders can share relevant topics to engage and teach team members lessons learned from their own careers and answer relevant L&D related questions. We also hope to use this forum for external speakers to teach topics on relevant skills for team members.

### Goals

1. Provide a forum where team members can hear from established leaders on how they've managed their career and lessons learned to share with the wider community.

### Examples of Call Topics

This list of topics can be used as a starting point for brainstorming content and connecting with individuals to feature during Learning Speaker Series calls. This is not an exhasutive list - please add new ideas to this list as they come up in each monthly call!

- L&D Initiatives
- Leading Teams
- Managing Remote Teams
- Managing Underperformance
- Crucial Conversations
- Coaching
- Leadership Best Practices
- Developing Emotional Intelligence
- Developing High Performing Teams
- Managing Conflict

### Past Learning Speaker Series calls

Check out recordings of previous Learning Speaker Series calls!

- 2020-11-19 [Building Trust with Remote Teams Learning Speaker Series](https://www.youtube.com/watch?v=hHMDY77upAE&feature=youtu.be)
- 2020-12-10 [Managing Burnout with Time Off](/company/culture/all-remote/mental-health/#rest-and-time-off-are-productive)

### Hosting a Learning Speaker Series call

The following process outlines steps for the L&D team to take when planning and implementing the calls.

Anyone or any team can host a Learning Speaker Series for the organization! If interested and there is a topic & speaker lined up, please fill out a [Learning & Development Request template](/people-group/learning-development/general/-/blob/master/.gitlab/issue_templates/learning-and-development-request.md) to begin the process.

**Planning Issue Template:** Open a `learning-speaker-series` issue template in the L&D [General Project](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/tree/master). Steps outlined below are also included in the issue template and are included here as reference.

1. Collaborate with exec team, mid-level people leaders, and other interested GitLab Team members to gauge interest and availability to be featured in the call. Be mindful to include diverse voices from across the GitLab team.
1. Determine if this is an internal or external speaker for the series call. If external speaker, ensure that the speaker has been validated and that the topic covered aligns with our values. External speaker presentations need to aling with how we deliver group conversations.
1. Post in the [#peopleops Slack channel](https://app.slack.com/client/T02592416/C0SNC8F2N/thread/C0SNC8F2N-1602618225.269200) to coordinate a Zoom meeting on the company calendar. This meeting should be set up to live stream to the GitLab unfiltered YouTube channel. Consider hosting the call at least 2 times to best accommodate for multiple time zones.
1. Test Zoom functionality with the speaker at least two business days before event.
    - If external speaker, ensure they have downloaded [Zoom](https://zoom.us/support/download).
    - Have speaker test [audio and video](/handbook/tools-and-tips/zoom/#how-to-test-audio-and-video-in-zoom).
    - Check that speaker will be able to [share a presentation](/handbook/tools-and-tips/zoom/#how-to-share-a-presentation-in-zoom) if necessary.
1. Send speaker calendar invite with all details for the call (including, but not limited to, Zoom link, agenda, slides, etc.).
1. Create slide deck for presentation. Make a copy of a previous month's presention in the [Continuous Learning Campaign Google Folder](https://drive.google.com/drive/folders/1d4ksJXBMrATATxN0QyJ4FA6hzchMNdvb?usp=sharing)
1. Open a feedback issue for questions and comments to be shared asynchronously.
1. Coordinate an announcement of the call in the #company-fyi Slack channel from Sid or another executive/manager who will be featured that month. The post should be shared 1 business day before the call. This post should include a link to the slide deck and coresponding issue. See notes below for a template that can be shared.

#### Text for CEO share in #company-fyi channel

`Join the Learning and Development team on [DATE] for the Learning Speaker Series Call. This month's call features [PERSON] and their experience with [BACKGROUND}. You can review the slide deck for the call [HERE], and post questions you might have in the call adenga doc [HERE]. Looking forward to seeing you there!`

## Learning & Development Quarterly Newsletter

The L&D team also hosts and develops a [quarterly newsletter](/handbook/people-group/learning-and-development/newsletter/) for the community.

## Mental Health Quarterly Newsletter

The L&D team publishes a [quarterly mental health newsletter](/handbook/people-group/learning-and-development/newsletter/mental-health-newsletter) for team members and the wider community.

## Take Time Out To Learn Campaign

[Focus Friday's](/handbook/communication/#focus-fridays) are a great benefit at GitLab. We try to schedule minimal meetings on Fridays to catch up on work and grow our skills professionally. Use Focus Fridays to take time out to learn. Team members can work with their manager through a [career development conversation](/handbook/leadership/1-1/#career-development-discussion-at-the-1-1) to determine what skills that want to grow in the future. Aspirations can be documented in an [individual growth plan](/handbook/people-group/learning-and-development/career-development/#internal-resources). 

From there, identify what will be needed to attain the new skills and consider using the [Growth and Development Benefit](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) or [expensing professional development opportunities such as coaching, worskshops, conferencces, self-service learning, etc.](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/#types-of-growth-and-development-reimbursements). Another option you can utilize to learn more is [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/). GitLab has a number of LinkedIn Learning licenses for team members. Block time off your calendar every week or month to devote to learning new skills. Share what you are learning with team members and in the #learninganddevelopment slack channel. 

Consider documenting the steps you are going to take learn new skills in the individual growth plan. Check in with your manager and ask for accountablity from them to help you stay aligned with goals. 


## Mini-Challenge

Mini-challenges are 1 week learning opportunities that utilitze a combination of GitLab issues, live learning sessions, and Slack to organize a group of learners around a specific topic. Mini-challenges are advertised to team members via slack and require interested learners to opt in. Participation in issue discussion questions and in live learning sessions (synchronous and asynchronous) are is tracked by the L&D team or challenge organizer. Upon completion of the challenge, participants are awareded a certification. The L&D team has used the mini-challenge model to engage learners on topics like psychological safety.

In addition to the use of GitLab, each mini-challenge includes a dedicated Slack channel to organize conversation, share new issue links, and provide space for team members to ask questions.

Examples of current and past challanges can be found in the [Learning and Development project](https://gitlab.com/gitlab-com/people-group/learning-development/challenges)

### GitLab Mini and Extended Challenges

All of GitLab's Learning and Development programs use the [GitLab tool to facilitate learning](https://gitlab.com/groups/gitlab-com/people-group/learning-development/-/epics/1). One way we do that is with the use of daily challenges. Daily challenges are a proven strategy for implementing new habits for our team members. Daily challenges can cover a range of topics, and they are intended to introduce behavior changes that will stick. In September of 2020, we launched a [Four Week Manager Challenge program](/handbook/people-group/learning-and-development/manager-challenge/) that consisted of daily challenges and live learning events to strengthen foundational management skills. In November of 2020, we launched a [One Week Challenge on Psychological Safety](/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge). 

**Sample structure of a challenge:**
- Length of challenge: 1 week (But can be for as long as 4 weeks)
- Sample challenge topic: Giving Feedback
- Daily challenges: 20-minute daily challenge (Monday through Friday)
- Live Learning: Optional live learning session at the end of the week to revisit the challenges covered
- Self-Reflection: At the end of the week, have participants complete a self-reflection exercise
- Certification/Badging: Option to award participants with a certification or badge


## Internal Learning Campaigns

Learning campaigns at GitLab have an asynchronous focus and are used to raise awareness around a specific topic or set of resources. These campaigns are inspired by the structure of a learning challenge but without the required engagement or tracking. For example, the L&D team is using a learning campaign structure to host a [mental health awareness week](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/156)

Learning campaigns use a combination of GitLab issues and Slack announcements to raise awareness and spark discussion. Following the campaign, the L&D team will aggregate comments and resources that have surfaced and document in the handbook. Learning campaigns can be used to build buzz around a live speaker series or to start conversation about a common question or issue that GitLab team members are facing.

## CEO Handbook Learning Sessions

GitLab's Handbook pages grow every day. Each page serves as GitLab's primary source of [learning and development material](/handbook/people-group/learning-and-development/#handbook-first-training-content). Throughout FY22, the L&D team and the CEO will hold recorded interactive learning sessions to analyze Handbook pages. The goal of the sessions will be to incorporate more video-based learning into the handbook.

Three types of CEO handbook learning sessions:

1. **Handbook Readout:** Bit-sized recording where the L&D team and the CEO review what is on the page. (5 minute video)
1. **Handbook Discussion:** Interactive discussion where L&D facilitates an engaging conversation with the CEO and e-group members. We openly discuss the concepts on the page and allow senior leaders to share best practices implementing them. (25 minute video)
1. **Handbook Interview:** The Learning and Development team will create new pages, create sub-pages, or update existing pages based on interview topics discussed during a targeted Q&A. The video will serve as the foundation for new learning content at GitLab.

The video below from Sid and L&D further explains the concepts:
<figure class="video_container"><iframe src="https://www.youtube.com/embed/cB301JaYnsw" height="315" width="560"></iframe></figure>Steps for L&D Team when setting up a CEO Handbook Learning Session:

1. Review topic relevant Handbook pages.
1. Identify pages that need video and content update.
1. Edit page with new content, create sub-page, send MR in the CEO channel with a proposal for an interactive handbook discussion.
1. Set up a meeting with CEO through EBA. Invite other e-group members if applicable.
1. Create an agenda with talking points and areas to emphasize during video recording with CEO
1. Hold a handbook learning session and ensure the discussion is fluid and interactive with open and honest dialogue.
1. Determine if L&D can create bite-sized videos with the content. Post bite-sized and long-form video on YouTube Unfiltered channel
1. Embed video on the related Handbook page.

List of CEO Handbook Learning Sessions:

1. [Common misperceptions about Iteration](https://www.youtube.com/watch?v=nXfKtnVUhvQ)
1. [No Matrix Organization](https://www.youtube.com/watch?v=E_wegGRv4mA)
1. [Making Decisions](https://www.youtube.com/watch?v=-by6ohMIi_M&feature=emb_title)
1. [Individual Contributor Leadership](https://www.youtube.com/watch?v=d0x-JH3aolM)
1. [Bias Towards Asynchronous Communication](https://www.youtube.com/watch?v=_okcPC9YucA&feature=emb_title)
1. [High Output Management](https://about.gitlab.com/handbook/leadership/high-output-management/#applying-high-output-management)
1. [Giving and Receiving Feedback](https://www.youtube.com/watch?v=vL864Zg2sm4&t=731s)
1. [Managing Underperformance](https://www.youtube.com/watch?v=-mLpytnQtlY&t=637s)

