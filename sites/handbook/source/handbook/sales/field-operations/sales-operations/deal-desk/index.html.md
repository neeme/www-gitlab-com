---
layout: handbook-page-toc
title: "Deal Desk Handbook"
description: "The Deal Desk team's mission is to streamline the opportunity management process while acting as a trusted business partner for field sales."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## **Welcome to The Deal Desk Handbook** 

The Deal Desk team's mission is to streamline the opportunity management process while acting as a trusted business partner for field sales. We are the first point of contact for sales support. 

### Key Focus Areas
    
*  Sales Support
*  Quote Configuration
*  Complex/Non-standard Deal structure
*  Month End / Quarter End Reconciliation

### Helpful Links

*   **Salesforce Reports and Dashboards**
    *   [Current Quarter WW Sales Dashboard](https://gitlab.my.salesforce.com/01Z4M0000007H7W)
    *   [Monthly Bookings Report](https://gitlab.my.salesforce.com/00O61000004Ik27)
    *   [Deal Desk Pending Opportunity Approvals Report](https://gitlab.my.salesforce.com/00O4M000004e0Dp)

*   **Frequently Used Handbook Pages**
    *   [Sales Order Processing](/handbook/business-ops/order-processing/)
    *   [Deal Desk Opportunity Approval Process](/handbook/business-ops/order-processing/#submitting-an-opportunity-for-deal-desk-approval)
    *   [Useful Company Information](https://gitlab.com/gitlab-com/finance/wikis/company-information)
    *   [Account Ownership Rules of Engagement](/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement)
    *   [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#annual-recurring-revenue-arr-and-salesforce)
    *   [Vendor Setup Form Process](/handbook/business-ops/order-processing/#how-to-process-customer-requested-vendor-setup-forms)
    *   [Security Questionnaire Process](/handbook/engineering/security/#process)
    *   [Troubleshooting: True Ups, Licenses + EULAS](/handbook/business-ops/business_systems/portal/troubleshooting/)
    *   [Licensing FAQ](https://about.gitlab.com/pricing/licensing-faq/)
    *   [Legal Authorization Matrix](/handbook/finance/authorization-matrix/)
    *   [Trade Compliance (Export/Import)](/handbook/business-ops/order-processing/#trade-compliance-export--import-and-visual-compliance-tool-in-salesforce)

*   **Other Resources**
    *   [Quote Approval Matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?ts=5d6ea430#heading=h.ag75fqu12pf0)
    *   [Billing FAQs and Useful Tips](https://gitlab.com/gitlab-com/finance/-/wikis/Billing-Team-FAQs-&-Useful-Tips)
    *   [Sample Order Form (Blank)](https://drive.google.com/open?id=1NB5KH7U4cucjiOjUdZrq94mYGzH6jG4f)

### **Sales Support**

#### Deal Desk SLA 

The Deal Desk team will do their best to respond to each request to '@sales-support' within 4 hours. Revenue generating, current quarter requests will take priority, especially during Month & Quarter End. If a task is not resolved within 24 hours it will be escalated (if necessary). 

| Type of Request | First Response | Resolution |
|----- | ----- | ------| 
| Basic Quote Assistance | 6 Hours | 8 Hours | 
| Ramp Deal | 6 Hours | 24 hours |
| Flat Renewal | 6 Hours | 24 Hours |
| IACV/ARR Review | 6 Hours | 24 Hours |
| Contract Reset / Co-Term | 6 Hours | 24 Hours | 
| RFP/Vendor Forms | 6 Hours | Dependent on AM |

### SLA for EoA Ramp Deal Requests

The Deal Desk team will respond to E0A Ramp requests as quickly as possible. However, due to the complexity involved in building ramp deals, the SLA for an EoA Ramp Deal Order Form creation is as follows:

| Subscription Term Start Date | First Response | Resolution |
|----- | ----- | ------| 
| < 30 days in the future | 4 Hours | 1 Business Day | 
| > 30 days in the future| 4 Hours | 3 Business Days |

A Deal Desk team member will respond to your chatter request with an estimate on when the Order Form will be complete. Please let us know if your customer requires the quote in advance due to lengthy procurement cycles or budget planning reasons - exceptions will be considered in cases of urgency, or to meet a customer's needs.

#### Deal Desk - A Global Presence

The Deal Desk team is located around the world and will be available during standard business hours within most regions - typically 9:00am - 6:00pm for the local time of the team member, as outlined below. 

**EMEA**
*  Marcsi Szucs - Budapest, Hungary
*  Donatela Cekada - Dublin, Ireland

 **AMER**
*  Jesse Rabbits - New York, NY 
*  Ashley Potter - New York, NY 
*  Cal Baker - Seattle, WA

**APAC**
*  Kriti D'Souza - Pune, India (Currently on leave from November 2020 to March 2021)

#### Salesforce Chatter Communication

Deal Desk's primary communication channel is Salesforce Chatter. When you chatter `@Sales-Support`, it will automatically create a case in the Deal Desk queue. 

Deal Desk team members monitor the queue throughout the day. For all cases directly related to opportunites, quoting/deal structure or booking a deal, Deal Desk will respond to a case within 6 hours, Monday-Friday, with the exception of National/Regional holidays. Resolution or escalation for these cases will occur within 24 hours. 

For cases related to Account Segmentation (ROE, Territory), account data, or account merges, Deal desk will reassign these cases to our Sales Operations team to review and resolve. 

To Chatter the DD team, tag `@Sales-Support`in Chatter on the related opportunity or account page and a short sentence on your request. If the Deal Desk team needs more information, we will follow up directly via Chatter. 

**Please do not tag Deal Desk team members directly in chatter or make a request through Slack direct message. Always uses `@Sales-Support` for SFDC requests or post `#sales-support` in Slack for general questions.** This ensures our team is working as efficiently as possible and that you are covered in case the DD team member who replied first is unavailable. If someone is working on a case, they will continue to support until the case is closed.  If an issue has been resolved, please chatter @Sales-Support to reopen a case.


#### Slack Communication

#### Primary Slack Channel

Use our Slack channel in case of general, non-record related requests and/or urgent questions: 
**#sales-support** [If the request is related to a quote, opportunity, or account - please chatter on the page in Salesforce instead of the Slack channel.] 

#### Slack Best Practices

**Please avoid contacting the DD team members directly via Slack.** Utlizing the `#sales-support` channel is best to ensure timely coverage, helps others who may have similar questions, and aligns with our **Transparency** value. 

In case of a specific opportunity or quote related question please use SF Chatter (see section [Salesforce Chatter Communication](#salesforce-chatter-communication))

#### Slack Announcements

Desk Desk process updates and announcements will be communicated via `**#sales**` and `**#sales-support**` Slack channels. 

#### Deal Desk Office Hours

Weekly Deal Desk Office Hours are scheduled each Wednesday at 12 PM EST. During Month End, Office Hours will take place on Monday, Wednesday, and Friday, scheduled in both AMER and EMEA time zones. Calendar invites will be sent to Sales-All Distribution group. Priority will be given to opportunities closing within the quarter. 

Supported topics include:
* Create or modifying a quote
* Quote approval acceleration
* IACV/ARR calculation
* Submitting an opportunity for close
* Validation/segmentation of closed opportunities
* And anything else to help drive opportunities closing within the quarter! 

#### Deal Desk AMA

Deal Desk AMA's are an opportunity to invite someone from Deal Desk to your local team call. We can help address any questions regarding quote processes, best practices, or unique deal structures. Think of this as an opportunity for your team to learn together on general topics for quoting or opportunity management. For specific questions related to in-flight opportunities, it is best to join [Deal Desk Office Hours](/handbook/sales/field-operations/sales-operations/deal-desk/#deal-desk-office-hours). 

Chatter or Slack #sales-support if you are interested in having Deal Desk join your team call! 

#### Proactive Opportunity Review

In an effort to proactively prevent common order issues (especially those that would require a customer to resign our order form) the Deal Desk team will conduct a manual review of all opps that meet the following criteria: 

- Close Date = Within Current Quarter
- Net ARR = $100k+ 
- Opportunity Stage - 4 OR 5 

Team members will review opps in their region each week. We are looking for anything that would prevent the opportunity from booking smoothly, including:

1. Check that quote is built correctly for the opportunity type and route to market (direct, channel, marketplace)
2. Review quote approval requirements are met
3. Review IACV / ARR (IACV review through FY21Q1)
4. Review PO Requirement. If the account requires a PO, chatter the rep with a reminder
5. Check with rep to determine whether any questions or additional needs, especially if you see something structured oddly on the opp/quote, or if it is a complex deal

**Review Cadence** 

The team will review opps on a monthly basis, as time allows throughout the work week. During the last month of any quarter, the review will be conducted weekly. 

### iACV + Renewals

#### Calculating iACV 

Note: As of FY22, iACV is no longer GitLab's primary bookings metric. 

To calculate iACV, please review the [iACV page of the handbook](/handbook/sales/sales-term-glossary/acv-in-practice). Alternatively, please chatter `@Sales-support` on the opportunity for assistance in calculating iACV. 

You can also use [this calculator](https://docs.google.com/spreadsheets/d/10hX1ZwTuxa-5PyJr30rTlATClzXmc8i0OunW1u-2D2I/edit#gid=0) to **estimate** the iACV for renewals.

#### Calculating ARR

To calculate ARR, please review the [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#annual-recurring-revenue-arr-and-salesforce) handbook page.

### **Zuora Quote Configuration Guide - Standard Quotes**

The following is intended as a step by step guide for opportunity owners or ISR's to learn how to create standard quotes for New Subscriptions, Amendments, and Renewal opportunities. 

#### New Subscription Quote

Follow this step by step guide for creating a **New Subscription** quote. Use a New Subscription quote when the customer is purchasing a brand new subscription **OR** if the customer is **renewing** for a term that is different from the original subscription (ex. original term 36 months, renewing for 12 months = new subscription quote). 

A.  Open the New Business opportunity and click the **“New Quote”** button.

B.  When prompted **select “New Subscription”** and click “Next.”

C.  Provide Quote, Account, and Subscription Term Details and click “Next.”
*   **Select “Sold To” and “Bill To” contacts.** Note that the “Sold To” contact will receive the EULA or License file via email. Note: Each contact record must have a complete address, if the address is not fully populated, you will need to update this before you can proceed with the quote. 
*   For **Channel deals,** populate the “Invoice Owner” and "Invoice Owner Contact" fields. For **Direct Deals** leave "Invoice Owner" and "Invoice Owner Contact" blank. 
    *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
    *   If an "Invoice Owner" does not auto-populate in the drop down, this means that a Billing Account does not yet exist for the partner. Chatter `@Billing-ops` on the Partner account and ask for a Billing account to be created. 
*   Populate **“Initial Term”** in months. (i.e. for a two-year deal, enter “24”). For standard deals, "Initial Term" should always match the SKU that you are quoting (i.e. Premium - 2 Year = Initial Term 24 Months). 
*   If the customer or reseller is based in the EU, enter the “VAT ID” number. This is required to book the order. 
*   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date” field** should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING MSA”** quote template.
*   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.

D.  Select **“Add Base Products”** from the drop-down menu on the “Edit Products and Charges” page. Select the correct product and click Next.
E.  Enter the product quantity, and adjust the discount or effective price as needed. Click Submit.
F.  **Order Form Generation**
*   If no discounts or special terms are requested, click “Generate PDF.”
*   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
    *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
*   **Order Form Manual Edits**
    *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging `@Sales-Support` in chatter. 

**To supplement these steps, review the [THIS TRAINING VIDEO](https://youtu.be/XqQC65-vJws) to see each step live for a New Subscription quote.**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 


#### Amend Subscription Quote


This quote type should be only used when **new users are being added to an existing subscription during the current subscription term.** This includes both additional licenses to existing products, and true-ups. This also includes any scenario where the products are being changed during the term - i.e., an upgrade from Premium to Ultimate. Amendment quotes cannot extend a subscription term. 

**A.  Add-On Quote Creation**
*   Open the Add-On opportunity and click the **“New Quote”** button.
*   Select the **existing billing account**.
*   When prompted **select “Amend Existing Subscription for this Billing account**, and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote, Account, and Subscription Term Details and click “Next.”
    *   The **Start Date** cannot be set before the subscription start date.
    *   The **End Date** will automatically be set to co-terminate with the existing subscription.
    *   The **Initial Term** should match the initial term of the New Business or Renewal subscription that precedes this quote. (i.e. if you’re amending the quote halfway through a 12 month term, the Initial Term should be 12, not 6.
    *   The **GitLab Entity** must be the same as it was on the initial deal you’re amending.
    *   For **Reseller deals,** populate the “Invoice Owner” and “Invoice Owner Contact” fields.
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
        *   ** We cannot change route to market through an Amendment quote**. If the original order was sold Direct, all subsequent amendments must also be Direct. If it was initially sold through a Partner, all amendments must go through the same partner for the duration of the subscription. 
    *   If the customer or reseller is based in the EU, enter the “VAT ID” number. This is required to book the order.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING MSA”** quote template.
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Under the **Edit Products and Charges** page, increase the existing license quantity to reach your total - i.e. enter the new total license count. This page will show all currently licensed products (marked “Original”), but the Order Form that generates will only show the added quantity and amount for the pro-rated period. 
*   **To add users to the existing subscription at a different price**, the new user licenses should be added as a separate product line. 
*   **True-Ups:** If you are quoting true-up users, click “Add Products” and wait for the next page to load. Then, click “Select” and choose “Add Add-On Products.” Select True-Up and click Next. Edit the quantity and effective price. Click “Submit.”
*   **Order Form Generation**
    *   If no discounts or special terms are requested, click “Generate PDF.”
    *   If the deal contains discounts that require approval, please submit the quote for approval using the button on the quote. The quote must be approved before the PDF can be generated.
        *   In the case of reseller deals, please obtain the approvals in Chatter based on the approval matrix.
    *   **Order Form Manual Edits**
        *   Please note that Sales reps may only generate PDF versions of the Order Form. If you require special wording or other manual edits, please make the request by tagging @Sales-Support in chatter.

**B.  Upgrade or Switch Products During the Subscription Term**
*   Create an “Amend Subscription” quote by following the steps in Section 2 (A) above.
    *   The **Start Date** should be the date of the product exchange. 
    *   On the **Edit Products and Charges** page, select “Add Products” and wait for the next page to load.
    *   Click “Select” and click “Add Base Products.”
    *   Select the **new** product type, and the correct SKU. Click Next.
    *   Select the **Remove** drop down button (which is not fully visible) next to the current product, which you are removing in lieu of the new product.
    *   Adjust the new product line - quantity, discount. Click “Submit.”
    *   Note: On the Order Form, the product being removed will display with a negative amount reflecting the credit for that product for the remainder of the subscription term.


**To supplement these steps, review [THIS TRAINING VIDEO](https://youtu.be/iTVbggacglo) to see each step live for an Amendment Quote:**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 


#### Renew Subscription Quote

This quote type should be used when the customer has reached the end of their subscription term and wishes to renew the subscription for another term. 

**A.  Standard Renewal**
*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “Renew Existing Subscription for this Billing account,** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Provide Quote, Account, and Subscription Term Details and click “Next.”
    *   The **Start Date** cannot be edited. This will be the true renewal date.
    *   The **End Date** will automatically be determined by the Renewal Term.
    *   Populate **“Renewal Term”** in months. (i.e. for a two-year renewal, enter “24”)
    *   Select the proper **GitLab Entity.**
    *   For **Reseller** deals, populate the **“Invoice Owner”** and **“Invoice Owner Contact”** fields.
        *   Ensure that the Invoice Owner Contact is connected to the correct reseller account - i.e. if the reseller is in Germany, the Invoice Owner Contact should also be based in Germany.
    *   If the customer or reseller is based in the EU, enter the **“VAT ID”** number. This is required to book the order.
    *   If there is a **signed MSA** between the customer and GitLab, the **“Existing GitLab Agreement Effective Date”** field should be populated with the date of the MSA’s signature. In addition, select the **“EXISTING MSA” quote template.**
    *   Note: Non-standard payment terms (Net 30 = standard) should be reflected on the quote object, and will require approval per the approval matrix.
*   Click Next, to enter the Products and Charges page.
    *   Note: The product lines from the Initial Term are already listed and will be marked “Original.” You may add new products, update the quantity on the original license, or remove the existing license. To add users to an existing license at a different price, the new user licenses should be added on a new product line.

**B.  Renewal Using a New Subscription Quote**

Certain renewals require a New Subscription quote. The most common scenario is if you are changing the term as of the renewal date - i.e. changing from an annual plan to a multi-year plan.

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select the **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the **New Subscription Quote** section above. 
    *   **Note: The Start Date must be the renewal date.** 
*   Click Next and update the products and fees per the steps above.

**To supplement these steps, review [THIS TRAINING VIDEO](https://youtu.be/kDZJW-ss5j4) to see each step live for an Renewal Quote:**

**Note:** If you cannot view the video, make sure you are logged in to GitLab Unfiltered. You can [learn more about how to log in or request access to GitLab Unfiltered here!](/handbook/marketing/marketing-operations/youtube/#unable-to-view-a-video-on-youtube) 


#### Co-Terming

Co-terming is when a customer wants to align two or more **separate subscriptions** with different end dates. Co-Terming is not the same as an Amendment quote where we are adding and prorating users for an **existing subscription.**


Ex). Customer has 2 separate subscriptions for Premium.

* Subscription 1 = 100 seats, expiring 2018-06-30
* Subscription 2 = 50 seats, expiring 2018-12-31

The most likely scenario is to co-term into the subscription ending last:

* On the renewal opportunity for subscription 1, create an amendment quote for subscription 2;
* The start date for the amendment will be 2018-07-01;
* Add 100 seats to the existing 50 and close as usual;

The other scenario is co-terming into the subscription ending first:

This requires two steps: first combining the subscriptions, then cancelling the leftover one.

* Create a new opportunity, with a quote amending subscription 1;
* The start date will remain unchanged, e.g. 2018-04-01;
* Add 50 seats to the existing 100 and close as usual;
* Once closed, create a new opportunity to cancel subscription 2 on the day before the start date of the previous quote e.g. 2018-03-31;

#### Quoting Guide: Starter/Bronze End of Availability + Tier Re-naming

**As of 10:00am ET on Tuesday, 2021-01-26, the following changes have been made to the quoting process in relation to the end of availability of Starter/Bronze:**

* Starter/Bronze SKUs will no longer be selectable for new subscription quotes. 
* Starter/Bronze new subscription deals quoted before 2021-01-26 will be honored and booked through and until the Quote Expiration Date provided that they have received CRO approval.
* Starter/Bronze SKUs will still be selectable for amendment and renewal quotes. Existing Starter/Bronze subscriptions can be amended to add users at the current list price, or renewed for one year at the current list price, until 2022-01-26. Any discounts applied to Starter/Bronze Add-Ons or Renewals will be subject to the [standard discount approval matrix](https://docs.google.com/document/d/1-CH-uH_zr0qaVaV1QbmVZ1rF669DsaUeq9w-q1QiKPE/edit?ts=5d6ea430#).

**Silver/Premium + Gold/Ultimate Legacy SKU Quoting Guide**

As of 10:00am ET on Tuesday, 2021-01-26, the following changes have been made to the quoting process in relation to the renaming of Silver and Gold:

Salesforce Guided Selling filters have been updated. Legacy SKUs have been removed where appropriate, resulting in a shorter, cleaner list of products that can be sold. Legacy SKUs from previous product deprecations have also been removed.

* All legacy Premium, Silver, Gold, and Ultimate SKUs will begin the deprecation process. **New SKUs have been created in place of these legacy SKUs:**

| New Rate Plan Name      | Legacy Name |
|-------------------------|-------------|
| SaaS - Premium          | Silver      |
| Self-Managed - Premium  | Premium     |
| SaaS - Ultimate        | Gold        |
| Self Managed - Ultimate | Ultimate    |

* **New Business:** New Subscription quotes can only be created using the new SKUs listed above. 
* **Add-Ons:** For existing subscriptions that contain legacy SKUs, all Amend Subscription quotes will use the existing legacy SKU. i.e. If you upsell an existing Gold subscription, your amendment quote and Order Form will still use the legacy Gold SKU.
* **Renewals:** For existing subscriptions that contain legacy SKUs, **all Renew Subscription quotes will require that the legacy SKU be removed from the quote, and that the new SKU be added to the quote in its place for the renewal.** When quoting a renewal, *please follow the steps below to change the SKU:*
  * First, click **Select Products**
  * On the **Edit Products and Charges** page, select "Add Products"
  * Click "Select" and then click "Add Base Products"
  * Select the new SKU. Click Next.
  * Select the **Remove** drop down button next to the legacy SKU.
  * On the new SKU, adjust the quantity and pricing as needed. Click "Submit."
  * Note: On the Order Form, the legacy SKU will appear as "Removed."

* **Legacy SKU Grace Period**: Any quotes/Order Forms created before 2021-01-26, on which a legacy SKU was used, will be accepted for booking. All quotes/Order Forms created on or after 2021-01-26 must follow the applicable quoting requirements for each quote type listed above.

**Quote Discount Approval Module Updates**

* The quote approval module has been updated to recognize the new Premium and Ultimate SKUs referenced in the table above.
* The quote approval module has been updated to recognize the [Starter/Bronze EoA Option 2 upgrade offer](https://docs.google.com/document/d/19T-ysFuEFWIlAv7Z9o1Q6-kVU9HKFb_nzGZ1uumpMLA/edit) (one year renewal/add-on upgrade).

#### Professional Services Quote

**A.  Create a standalone Professional Services Opportunity.**

*   Create a New Business Opportunity and select “Professional Services Only” under “Opportunity Record Type.” **Note: Professional Services products must always exist on a separate, standalone opportunity.**

**B.  Creating a Professional Services Quote with Standard Product SKUs**

*   Create a new subscription quote under the Professional Services opportunity by following the steps above under “new Subscription Quote.” Select the required SKUs for Professional Services.
*   More information on [Professional Services SKUs](/handbook/customer-success/professional-services-engineering/#professional-services-skus) 

**C.  Creating a Professional Services Quote with Custom SKUs**

*   Create a new subscription quote under the Professional Services opportunity by following the steps above under “new Subscription Quote.” Select **GitLab Service Package.** Update the price to reflect the price on the SOW.

**D.  Before submitting a Professional Services Opportunity for Closure:**

*   Please note that the following items must be attached to the opportunity before it can be Closed Won:
    *   SOW signed by **both the customer and Brian Robins.****
    *   Cost Estimate spreadsheet (Link provided by the PS team)
    *   For a custom service package (Custom SOW), a fully executed SOW and Cost Estimate are required
    *   For [standard professional services skus](/handbook/customer-success/professional-services-engineering/#current-skus), only a signed order form is required 
    *   For opportunities with both - standard professional services skus AND a custom service package - then both a fully executed SOW and Cost Estimate is required 

#### How to Clone an Existing Quote

If you'd like to save time by cloning an existing quote, you can do so by doing the following:

1. On the Quote Detail page of the quote you want to clone, click Clone Quote.
1. On the Quote Clone Configuration page, select the following options:
    * Clone Products: Select to clone the products associated with the quote. This option only applies to the New Subscription quotes. Ensure that the products associated with the quote are maintained in the current version of the product catalog.
    * Maintain Quote: Select to be directed to the first step in the quote object workflow that allows you to edit the newly cloned quote. The flows are configured based on the quote type, i.e., New Subscription, Amendment, and Renewal, in the quote object workflow.
1. Click Next.
    * If you selected the Maintain Quote option, you are redirected to the first step in the Quote Wizard of the newly cloned quote.
    * If you did not select the Maintain Quote option, you are redirected to the Quote Detail page of the newly cloned quote.
1. Please note that you cannot clone the products on an amendment (add-on or renewal quote.)

#### How to Create a Draft Proposal

Follow the standard process for [quote creation](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes). The Quote Object **does not** need to be approved before generating a Draft proposal. 

1. Click Edit Quote. 
2. Select the Draft Quote Template. Save. 
3. Click Generate PDF. A Draft Proposal PDF will be attached to the opportunity in the Notes & Attachments section. 

**Important Notes** 
- A Draft Proposal PDF is not an Order Form. All quotes must go through the applicable approval process before you can generate an Order Form. Draft Proposals are not guaranteed approval.
- A Draft Proposal PDF will not be accepted in place of an Order Form under any circumstance. 
. To generate a legitimate order form, you must update the Quote Template from the Draft selection to relevant order form template for the opp

### **Non-Standard Quotes**

Occasionally an opportunity will require a unique structure that is outside of the normal quote format. Examples of these scenarios are listed below. Deal Desk will partner with the Account Owner to structure the opportunity and provide guidance on creating the quote. Please chatter on the opportunity if you need assistance with one of these scenarios! 

#### Contract Reset

Contract Resets are used to perform an "Early Renewal" - i.e. start a new 12 month subscription before the renewal date. They can also be used if a customer needs to change a billing date or would like to change the term length, mid term. 

*   Open the Renewal opportunity and click the **“New Quote”** button.
*   Select **existing billing account.**
*   When prompted **select “New Subscription for this Billing account,”** and select the appropriate subscription. 
    *   Note: If there are multiple billing accounts or subscriptions available, and you are unable to determine which to choose, please ask @Sales-Support via Chatter.
*   Fill out the necessary information on the quote per the instruction under the New Subscription Quote section above. 
    *   **Note: The Start Date should be the new subscription term’s start date, or the “Early Renewal” date.**
*   Click Next and update the products and fees per the steps above.
*   **IMPORTANT:** Next, tag `@Sales-Support` in Chatter on the Renewal Opportunity to create a credit opportunity and quote to cancel the existing subscription, which in this scenario is being replaced with the new subscription. Deal Desk will then manually generate a Contract Reset Opp and Order Form to add the credit line into the order form once the quote has been fully approved.
*   Deal Desk will attach a PDF of the consolidated quote to the opportunity 
*   Sales team will send the order form to the customer for signature
*   Upload the signed order form to the renewal opp and to the credit opp as well and submit both opps for approval

On the main contract reset opportunity, populate the "Opportunity Category" field with "Contract Reset." On the associated credit opportunity, populate the "Opportunity Category" field with "Credit." 

All Contract Reset opportunities will be classified as a "Renewal" and will be subect to Renewal ACV, which will impact overall IACV depending on the scenario. 

For more information on ARR calculation for Contract Resets, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-contract-resets).

#### Multi-Year Deals

A.  Note: In the case of multi-year deals, the Initial Term” should be updated to reflect the number of months in the term - 24, 36, 48, etc. Also, select the correct Product (i.e. for a 2 year SaaS - Premium deal, select “SaaS - Premium - 2 Year”). Multi-year deals that do not utilize the correct SKU will be rejected. 

B.  If annual payments are requested for multi-year deals, use the 1 Year product. Note that annual payments must be approved in chatter by Paul Machle.

C.  Note: If annual payments are approved, create a separate opportunity and quote for each year of the subscription. A three-year deal with an annual payment schedule will have 3 separate opportunities and quotes reflecting each year of the subscription. Work with @Sales-Support in Chatter if needed. The "Payment Schedule" field on each opportunity should be populated with the value "Annual Payments."

For more information on ARR calculation for Multi-Year Deals, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#how-net-arr-is-calculated-for-standard-deals).

#### Ramp Deals 

A ramp deal is when a customer prepays for users that will be fulfilled on a set schedule throughout the duration of the subscription term. In a Ramp, users are prorated from when they will be added. The customer pays upfront for all users planned in the ramp schedule. For [Amendment quotes](/handbook/sales/field-operations/sales-operations/deal-desk/#amend-subscription-quote), the customer adds users throughout the subscription with no set schedule. 

Ramp deals are limited to multi-year deals, with 12 month ramp periods. See [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-ramp-deals) for more information.

</div>
</div>

#### How to Create a Ramp Deal

A.  To create a ramp deal, tag @Sales-Support in chatter on the opportunity. **Provide the following information for each ramp period:**
*   Start date/term length
*   Proposed ramp schedule
*   Bill To and Sold To Contact
*   Product, quantity, discount
*   Payment Terms (i.e. Net 30)
*   Any other requests (i.e. Price Lock, true up language, etc.)

B.  Request template:
*   Deal Desk has created a [G Sheet template](https://docs.google.com/spreadsheets/d/1ho_ndKIZDvgdWOn873XONK2oc1tLloVJwlnpxgHYjiE/edit#gid=0) for Sales reps to enter ramp information.
    *   To use this template, copy the template to your own Drive, enter the information, and share your G Sheet with @Sales-Support in Chatter.

C.  Deal Desk will create the quotes and Order Form.

On the opportunity, populate the "Opportunity Category" field with "Ramp."

For more information on ARR calculation for Ramp Deals, see [ARR in Practice](https://about.gitlab.com/handbook/sales/sales-term-glossary/arr-in-practice/#calculating-net-arr-for-ramp-deals).

#### Miscellaneous (But Important) Information related to Quotes

A.  **To add users to an existing license at a different price**, please add the users on a new, separate product line.

B.  **To create a true-up/add-on quote for a multi-year deal**, please add **both** the true-up and increase the license count by the same number of users. Note that the user number cannot decrease during the term of a multi-year deal - i.e. in the case of a three-year deal, if the customer exceeds the 100 license count by 25 users, (1) True-Up SKU with 25 users, and (2) increase the license quantity from 100 to 125.

C.  **If the customer signs a renewal quote, but a true-up is required before the renewal date**, create an add-on opportunity from the closed renewal opportunity, use the same start date as the renewal, and add the necessary true-up.

D.  If you have **multiple quote objects** under one opportunity, the quote you are using **must be marked Primary.**

### Opportunity Categorization

As of October 1, 2020, the following fields will be maintained by Deal Desk to distinguish standard vs. non-standard opportunities:

#### Opportunity Category

*  Standard (Default Value)
   *  _Definition: A straightforward sale with no custom deal elements._
*  Ramp
   *  _Definition: A single deal with multiple individual ramp periods._
*  Contract Reset
   *  _Definition: A new subscription that replaces an existing subscription, where the existing subscription is canceled prior to its end date._
*  Credit
   *  _Definition: A cancellation related to a contract reset._
*  Decommission
   *  _Definition: A deal that is being debooked in full or in part, where all related commission/billing/revenue is being removed._
*  Decommissioned
   *  _Definition: A deal that has been removed from record, and is no longer active due to a subsequent decommission opportunity._
*  Internal Correction
   *  _Definition: An internal correction to a previously booked opportunity, where commission/billing/revenue figures are not altered._

#### Opportunity to Decommission

*  This is a lookup field, where the name of the original opportunity (now being decommissioned) is entered. This field is required by validation rule if Opportunity Category = Decommission. Upon saving, the linked opportunity will automatically be categorized as "Decommissioned" via process builder.

#### Payment Schedule

*  Prepaid (Default Value)
*  Annual Payments
*  Custom Payment Schedule

#### Opportunities Requiring Multiple Invoices

If an opportunity requires multiple invoices due to a specific professional services delivery schedule or approved annual payment terms, a separate opportunity is required for each invoice period. If there is no difference in number of seats or price across the years only one subscription and quote would be required (ie. Invoice Only opps do not require a quote object).

**Opportunity Structure**

* Invoice Only Opp type should be New Business
* Each individual opportunity will require a quote object that is equal to the amount being invoiced
* All products, dates, and contacts should match the original opp / quote
* Build Invoice Only quote objects as a "New Subscription" quotes

**Invoice Amounts** 

If all payments associated with the opportunity are equal (ex. 3 payments of $10,000) the quote on the original opporunity must reflect the entire opportunity term. 

**Ex.** 

3 Year Subscription worth $30,000, broken out into 3 equal annual payments of $10,000. 
* Primary Opp Quote - 3 Year New Subscription Quote (using the 1 Year Product SKU). Term Length should be 36. 
* Invoice Only Opp - Year 2 - Update the Amount field to reflect the total to be invoiced. A quote object is not required.
* Invoice Only Opp - Year 3 - Update the Amount field to reflect the total to be invoiced. A quote object is not required.

If the payment amounts or user count per year are not identical, (ex. Year 1 - $15,000 Year 2 - $10,000, Year 3 $5,000)- then the original opporunity quote should only reflect the first year (invoice period) of the subscription. 

**Ex.** 

3 Year Subscription worth $30,000, broken out into 3 payments. Year 1 - $10,000, Year 2 - $7,000, Year 3 $13,000
* Primary Opp Quote - 3 Year New Subscription Quote (using the 1 Year Product SKU). Term Length should be 36. 
* Invoice Only Opp - Year 2 - Update the Amount field to reflect the total to be invoiced. A quote object is not required. (Use the same SKU as the original opp, update initial term to 12)
* Invoice Only Opp - Year 3 - Update the Amount field to reflect the total to be invoiced. A quote object should be created to reflect the total amount to be invoiced. (Use the same SKU as the original opp, update initial term to 12)

**Ex.** Professional Services Deal -Opportunity amount $300,000. 4 Deliverables are outlined in the Custom SOW to the customer, each deliverable includes a different date for delivery. This requires 4 opportunities because the customer will be invoiced after completion of each deliverable.


*  Opp 1 (Original Opportunity) - First Deliverable - $50k in services
*  Opp 2 (Cloned from Original) - Second Deliverable -$50k in services
*  Opp 3 (Cloned from Original) - Third Deliverable - $100k in services
*  Opp 4 (Clone from Original) - Fourth Deliverable - $100k in services

To create opportunities requiring multiple invoices: 

*  Clone the original forecasted opportunity
*  Update the opportunity name to include “Deliverable # and the Date of delivery. The close date should be the same for all opportunities (if SOW and Cost Estimate was received on 1/1/2020, all opportunities should show 1/1/2020 as the close date even if it will be invoiced in a future period).
*  Create a quote reflecting the value of the Deliverable. Build out each quote using the same SKU and discount (if applicable). The start date should match the delivery date outlined in the SOW.
*  The quote should follow the standard quote creation process (link to quote building process). Set the quote as primary. Repeat for all opportunities. The sum amount of all opportunities should equal the total amount for the SOW.
*  Attach the signed SOW and Cost Estimate link to **each opportunity.**
*  Submit the Opportunity for Deal Desk/Billing approval. The Billing team will flag each deliverable opportunity for future invoice periods.

Note: The "Payment Schedule" field on each opportunity should be populated with the value "Annual Payments" or "Custom Payment Schedule," whichever applies.

**Add-Ons to Opportunities with Multiple Billing Events**

- Scenario: The customer has a two-year subscription, paid annually. Each year's invoice amount is $100,000. 6 months into the subscription, the customer would like to add 100 users, totalling $75,000 over the remaining 18 months of the agreement.
  - In scenarios where we are amending a subscription where there are one or more outstanding out-year payments, one quote should be provided, where the base product is amended as appropriate.
  - How to read the Quote Subtotal: In these scenarios, the quote's subtotal will be higher than anticipated, as the subtotal will account for (1) the add-on you're currently quoting, as well as (2) the year 2 payment (which is scheduled and hasn't been invoiced yet).
  - How to prepare the Order Form: In these scenarios, Deal Desk will manually prepare the Order Form by (1) Generating the Order Form, (2) subtracting the oustanding year 2 payment from the Order Form Grand Total, and (3) adding annual payment language and totals to the Order Form. 
  - How to prepare the Opportunities: In these scenarios, the primary opportunity will represent the remainder of the year in question. For each year remaining in the agreement, Deal Desk should (1) create a debook opportunity to debook the current out-year Invoice opportunity, and (2) create a rebook opportunity to correctly rebook the out-year Invoice opportunity with the new, increased out-year Invoice total (representing the original Invoice amount PLUS the add-on Amount for that future period)
  - In these scenarios
    - Example: From the scenario above, the quote's subtotal would read $275,000 because it includes (1) the $75,000 Add-On, and (2) the outstanding $100,000 Year 2 Invoice/Payment. 
    - Order Form: Deal Desk would (1) Change the Grand Total to $75,000, (2) Add Annual Payment language, and (3) in the table, write that $25,000 is due upon receipt (represents the add-on amount for the remainder of the first year), and that $50,000 is due on the same date as the outstanding Year 2 invoice. 
    - Opportunities: There would be an existing Year 2 Invoice Opportunity with an Amount of $100,000. Deal Desk would debook that opportunity. Deal Desk would then rebook that opportunity with an Amount of $150,000.
- Real-Life Example: https://gitlab.my.salesforce.com/0064M00000YQO1c?fId=0D54M0000465HEc

#### Zuora Quote Template Updates 

The Deal Desk team owns and manages any updates made to all quote templates. If you have a suggestion for an improvement to our existing quote templates, please [open an issue here](https://gitlab.com/gitlab-com/sales-team/field-operations/deal-desk)  

Include the following in the issue:

* Proposed Improvement
* Net New template or Adjusting an Existing Template? 
* Business Impact 

<details>
<summary markdown='span'>
  Zuora Template Resources
</summary>

In order to update quote templates that are used in Salesforce, and pulled in from Zuora, please reference the below resources provided by Zuora. 
1. [General overview to update quote templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates)
1. [Leveraging mail merge fields to update templates](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/C_Customize_Quote_Template_using_Word_Mail_Merge) - This must be completed in Microsoft word and saved accordingly
1. [Reference the merge fields that are supported](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/K_Supported_Merge_Fields_for_Quote_Templates_and_Mapping_to_Salesforce#Charge_Summary.Quote_Rate_Plan_Merge_Fields)
1. [How to displaty multiple quote charges in a table](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings/Customize_Quote_Templates/E_Customize_Quote_Templates_Using_Microsoft_Word_Mail_Merge)
1. [Uploading to Zuora and connect to Salesforce](https://knowledgecenter.zuora.com/CA_Commerce/C_Zuora_Quotes/CB_Zuora_Quotes_Configuration_Settings/D_Quote_Template_Settings) 

</details>


#### How to Process a Professional Services SOW Amendment (Change Order)

Occasionally, changes will be made to a Custom SOW after an opportunity has been booked. These changes could include delivery (invoice) schedules or types of services delivered. The total value of the opportunity **should not change.** If the total value of an opportunity changes, this will require an internal refund and rebooking of the order. 

If the total value of the opportunity has not changed:

*  Clone the original Closed-Won opportunity. Update the opportunity name to include “Amended SOW”.
*  Update the Original Closed-Won opportunity as duplicate, linking to the Amended SOW Opportunity.
*  Build a quote identical to the original closed-won opportunity. Update the quote to primary.
*  Attach the original SOW, the Amended SOW, and link to Cost Estimate to the Amendment Opportunity.
*  Submit the Opportunity for Deal Desk/Billing Approval.
*  The Opportunity Close Date for the Amended SOW should be manually updated to match the original Closed-Won opportunity.


#### Quote Entity Information

Note that the GitLab entity information will be populated via the following rules. This table is based on the [ISO-2 billing country code](http://www.nationsonline.org/oneworld/country_code_list.htm) of the direct customer or reseller we are delivering invoices to:

<table>
	<thead>
		<tr>
			<th>GitLab Entity</th>
			<th>Direct Customers and Unofficial Resellers</th>
			<th>Authorised Resellers</th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td>BV (NL)</td>
			<td>NL</td>
			<td rowspan="5">NOTE: Currently the default invoice entity is BV in the quoting tool.
				<br />
				<br />For new resellers: GL entity should be determined based on the signed reseller agreement.
				<br />
				<br />For old resellers: check the entity under the account in Zuora
			</td>
		</tr>
		<tr>
			<td>GmbH (DE)</td>
			<td>DE</td>
		</tr>
		<tr>
			<td>Ltd (UK)</td>
			<td>UK</td>
		</tr>
		<tr>
			<td>Inc. (US)</td>
			<td>All customers outside of NL, DE, UK, AU</td>
		</tr>
		<tr>
			<td>PTY LTD (AU)</td>
			<td>AU</td>
		</tr>
	</tbody>
</table>


**Note**: all initial web direct subscriptions ordered through the portal are placed on the US entity. (Clarification: however if the initial order was invoiced by the DE entity -through a sales assisted order- and customer orders an add-on via the portal, the add-on will be invoiced by DE as well.)

**Important**: in case of add-ons, the add-on quote/order form must reflect the same invoice entity that was on the initial/base deal.

#### Merging Accounts and Contacts

##### How to Determine if an Account is a Duplicate

1. Review DataFox / domain name - At minimum, the domain name should be a match on both accounts. You can also review Billing and Ship to addresses to confirm if it’s a duplicate. 

1. Open tabs of the accounts you want to merge. You can merge up to 3 accounts at a time. 
1. Not required, but helpful. Change the Name of both accounts to something that you can find easily. 

    Example - You need to merge CustomerCentral with Customer Central 
    Add 'Merge' to the end of the Account to be merged
    Account 1: CustomerCentral MERGE
    Account 2: Customer Central 
	 

1. Click on the Accounts tab in Salesforce. 
1. Scroll down, in the bottom right hand corner of the page, click “Merge Accounts” 
1. Search the name of the Accounts you want to merge - following the example above - in the search bar type xCustomer
1. Select the checkbox on the left of each account to merge, click Next. 
1. Review this section carefully. It’s best practice to keep the most accurate information when you merge accounts. You will be able to select one record to move the information into. 

1. On the top of the Merge Page you can Select which account URL you want to keep. If there is an existing Billing account/Subscription on the account it should be the master account! 
1. Once you have reviewed the information, click “Merge”. 

##### How to Update Opportunity IDs in Zuora

After an account is merged it is important to update the Zuora Billing account Opp ID. If this is not updated, Zuora cannot sync existing billing or subscription information to new quotes. 

1. Go to Zuora, under the Customers tab, search the account name - 
1. Click on the accounts that were merged 
1. Ensure that the CRM Account ID shows a hyperlinked account name with a green check mark. 
1. If it shows a Yellow exclamation mark, go to the primary account that is left from the merge. Copy the last 15 characters in the account page URL and paste into the CRM Account ID field in Zuora. Click Save. 

If Zuora successfully maps to the correct account, a green check box will appear.


##### Merging Contacts 

**NOTE** To merge a Lead with an existing contact, click "Convert Lead" - this is the SDR/Account Owner responsibility, not Deal Desk. 

1. Go to the Account page in Salesforce. 
    * Note, both contacts must be under the same Salesforce account in order to be merged. If they are not under the same account, change the account on the Contact level to the correct one. 
1. Navigate to the contacts section on the Account, click Merge Contacts. 
1. Select the contacts you need to merge (you can select 3 records at a time) 
1. Best practice - confirm that the email address is a duplicate before merging. 

##### Yikes, I merged something that I was definitely not supposed to! HALP. 

Never fear! Deal desk is here! All merged records go to the recycle bin, chatter at sales-support for assistance. The record will be retrievable from the recycle bin for 2 weeks before it’s permanently deleted. Dun, dun, dun. 
