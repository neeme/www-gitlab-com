---
layout: handbook-page-toc
title: "Sales Strategy and Analytics Handbook"
description: "To drive sales success by providing data, reporting, analytics, and actionable insights to leadership across GitLab"


---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the Sales Strategy & Analytics Handbook

### Charter

To drive sales success by providing data, reporting, analytics, and actionable insights to leadership across GitLab

### Working with Sales Strategy

Create an issue in the [analytics project](https://gitlab.com/gitlab-com/sales-team/field-operations/analytics/issues) and use the `Sales Strategy` label

### Team Members

| Team Member | Role | GitLab Handle |
| ------ | ------ | ------ | 
| Jake Bielecki | Sr Director Sales Strategy | [@jakebielecki](https://gitlab.com/jakebielecki) | 
| Alex Cohen | Director GTM Planning & Operations | [@alex.cohen](https://gitlab.com/alex.cohen) | 
| Matt Benzaquen | Sr Manager Sales Strategy | [@mbenza](https://gitlab.com/mbenza) | 
| Melia Vilain | Sr Sales Analytics Analyst | [@mvilain](https://gitlab.com/mvilain) |
| David Mack | Sr Sales Analytics Analyst | [@DavidMack](https://gitlab.com/DavidMack) |
| Noel Figuera | Sr Sales Analytics Analyst | [@nfiguera](https://gitlab.com/nfiguera)  |

### Sales Headcount Change Management Process

Please find instructions on how to initiate a Sales Headcount change [here](/handbook/sales/field-operations/sales-strategy/sales-headcount)

### Projects

You can find information about specific projects [here](/handbook/sales/field-operations/sales-strategy/projects)

### FY22 Field Planning

Placeholder for the FY22 Planning file
